Currency Exchange reference app that:
===================================================
* utilizes Kotlin, RxJava2, Dagger2, Retrofit2, Jetpack (ViewModel, Navigation etc.)
* downloads .json with a currency exchange rates from https://api.exchangeratesapi.io/latest?base=EUR
* updates exchange rates every second
* well-considered project structure, clean code and logic is highly valued

Demo:
===============================================
* Screencast: https://drive.google.com/open?id=1fUO6aZ_LqzVI7TeKPJgx0FRfZFtWMYde
* Apk: https://drive.google.com/open?id=1z_P3kGxnFJThgdFgtG8QDOjSdI3ZUIoJ

App architecture inspired by:
===============================================
* Reactive Apps with Model-View-Intent
http://hannesdorfmann.com/android/mosby3-mvi-1
* Managing State with RxJava by Jake Wharton
https://www.youtube.com/watch?v=0IKHxjkgop4
* Reark = RxJava architecture library for Android
https://github.com/reark/reark

License
=======

    The MIT License

    Copyright (c) 2013-2020 reark project contributors

    https://github.com/reark/reark/graphs/contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.