package io.bifri.currencyexchange.ui.main.converter


import android.content.Intent
import android.widget.TextView
import androidx.navigation.fragment.NavHostFragment
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.robotium.solo.Solo
import io.bifri.currencyexchange.R
import io.bifri.currencyexchange.ui.main.MainActivity
import io.bifri.currencyexchange.widgets.ConverterBaseCurrencyRowModel
import io.bifri.currencyexchange.widgets.ConverterCurrencyRowModel
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.math.BigDecimal


private const val CREATE_ACTIVITY_WAIT_TIME = 4_000
private const val RENDER_WAIT_TIME = 4_000

@RunWith(AndroidJUnit4::class)
class ConverterFragmentTest {

    @Rule @JvmField val activityTestRule: ActivityTestRule<MainActivity> =
            ActivityTestRule(MainActivity::class.java, false, false)
    private lateinit var solo: Solo

    @Test fun converterFragmentRenderTest() {
        val instrumentation = InstrumentationRegistry.getInstrumentation()
        val intent = Intent()
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val mainActivity: MainActivity = activityTestRule.launchActivity(intent)
        val soloConfig = Solo.Config()
        solo = Solo(instrumentation, soloConfig, mainActivity)
        solo.waitForActivity(MainActivity::class.java, CREATE_ACTIVITY_WAIT_TIME)

        val converterFragment = mainActivity
                .supportFragmentManager.fragments.filterIsInstance<NavHostFragment>()[0]
                .childFragmentManager.fragments.filterIsInstance<ConverterFragment>()[0]
        UiThreadStatement.runOnUiThread {
            converterFragment.render(convertState)
        }

        solo.sleep(RENDER_WAIT_TIME)

        checkBaseCurrencyCode()
        checkCurrencyCodes()
    }

    private fun checkBaseCurrencyCode() {
        val baseCurrencyCode = solo.getCurrentViews(TextView::class.java)
                .first { it.id == R.id.textViewConverterBaseCurrencyRowCurrencyCode }
                .text
        val correctBaseCurrencyCode = "EUR"
        assertEquals(correctBaseCurrencyCode, baseCurrencyCode)
    }

    private fun checkCurrencyCodes() {
        val currencyCodes = solo.getCurrentViews(TextView::class.java)
                .filter { it.id == R.id.textViewConverterCurrencyRowCurrencyCode }
                .map { it.text }
        val correctCurrencyCodes = listOf("CAD", "USD")
        assertEquals(correctCurrencyCodes, currencyCodes)
    }

    private val convertState get() =
        ConverterState(
                baseCurrency = ConverterCurrenciesAdapterBaseCurrencyItemModel(
                        ConverterBaseCurrencyRowModel(
                                "EUR",
                                R.drawable.flag_eu,
                                BigDecimal("1234.56")
                        )
                ),
                currencies = listOf(
                        ConverterCurrenciesAdapterItemModel(
                                ConverterCurrencyRowModel(
                                        "CAD",
                                        R.drawable.flag_ca,
                                        BigDecimal("1634.56"),
                                        BigDecimal("1.35"),
                                        "EUR"
                                )
                        ),
                        ConverterCurrenciesAdapterItemModel(
                                ConverterCurrencyRowModel(
                                        "USD",
                                        R.drawable.flag_us,
                                        BigDecimal("1334.56"),
                                        BigDecimal("1.23"),
                                        "EUR"
                                )
                        )
                )
        )

}