package io.bifri.currencyexchange.ui.main.converter

import io.bifri.currencyexchange.base.viewmodel.BaseRxViewBinder
import io.bifri.currencyexchange.exception.ExceptionService
import io.reactivex.disposables.CompositeDisposable

class ConverterFragmentVB(
        view: ConverterFragment,
        viewModel: ConverterFragmentVM,
        exceptionService: ExceptionService
) : BaseRxViewBinder(exceptionService) {

    override fun bindInternal(cd: CompositeDisposable) {

    }

}