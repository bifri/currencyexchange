package io.bifri.currencyexchange.ui.main.converter

import io.bifri.currencyexchange.network.response.latestrates.LatestRatesResponse
import org.junit.Assert
import org.junit.Test
import java.math.BigDecimal

class LatestRatesResponseTest {

    @Test fun latestRateResponseTest() {
        val latestRatesResponse = LatestRatesResponse(
                "EUR",
                "2020-05-20",
                mapOf(
                        "PLN" to BigDecimal("1.11"),
                        "CAD" to BigDecimal("1.23"),
                        "AUD" to BigDecimal("1.11"),
                        "USD" to null
                )
        )
        val sanitizedRates = mapOf(
                "AUD" to BigDecimal("1.11"),
                "CAD" to BigDecimal("1.23"),
                "PLN" to BigDecimal("1.11")
        )
        Assert.assertEquals(sanitizedRates, latestRatesResponse.ratesNonNull)
    }

}