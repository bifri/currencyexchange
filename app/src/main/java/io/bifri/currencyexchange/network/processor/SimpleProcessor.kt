package io.bifri.currencyexchange.network.processor

import io.bifri.currencyexchange.exception.ApiException
import io.bifri.currencyexchange.network.paymentsservice.ExchangeRatesApi
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers.io

class SimpleProcessor(
        private val apiExceptionProcessor: ApiExceptionProcessor,
        private val exchangeRatesApi: ExchangeRatesApi
) {

    fun <T : Any> loadData(
            apiCall: ExchangeRatesApi.() -> Single<T>
    ): Observable<Result<T>> = exchangeRatesApi.apiCall()
            .map {
                @Suppress("USELESS_CAST")
                Loaded(it) as Result<T>
            }
            .toObservable()
            .startWith(Loading())
            .doOnError(apiExceptionProcessor::processApiException)
            .onErrorReturn { LoadingError(ApiException(it)) }
            .subscribeOn(io())

}