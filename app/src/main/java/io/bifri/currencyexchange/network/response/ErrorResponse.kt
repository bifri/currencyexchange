package io.bifri.currencyexchange.network.response

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * {"error":"Base 'aaa' is not supported."}
 */
data class ErrorResponse @JsonCreator constructor(
        @JsonProperty("error") val error: String?
)