package io.bifri.currencyexchange.network.paymentsservice

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

private const val CONNECT_TIMEOUT = 30_000L
private const val READ_TIMEOUT = 30_000L
private const val WRITE_TIMEOUT = 30_000L

class OkHttpClientGetter(
        private val interceptors: Collection<Interceptor>
) {

    val okHttpClient: OkHttpClient get() = OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
            .apply { interceptors().addAll(interceptors) }
            .build()

}