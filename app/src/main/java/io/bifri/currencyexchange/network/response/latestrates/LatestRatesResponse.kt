package io.bifri.currencyexchange.network.response.latestrates

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

data class LatestRatesResponse @JsonCreator constructor(
        @JsonProperty("base") val base: String?, // EUR
        @JsonProperty("date") val date: String?, // 2020-05-20
        @JsonProperty("rates") val rates: Map<String, BigDecimal?>? // "AUD":1.6653
) {
    @Suppress("UNCHECKED_CAST")
    val ratesNonNull: Map<String, BigDecimal> = (
            rates?.filterValues { it != null } as Map<String, BigDecimal>?
            )
            ?.toSortedMap()
            ?: emptyMap()

}