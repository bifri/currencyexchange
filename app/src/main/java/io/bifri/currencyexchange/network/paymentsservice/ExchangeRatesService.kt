package io.bifri.currencyexchange.network.paymentsservice

import io.bifri.currencyexchange.network.response.latestrates.LatestRatesResponse
import io.bifri.currencyexchange.util.HEADER_ACCEPT_APPLICATION_JSON
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ExchangeRatesService {

    @Headers(HEADER_ACCEPT_APPLICATION_JSON)
    @GET("latest") fun latestRates(
            @Query("base") baseCurrency: String
    ): Single<LatestRatesResponse>

}