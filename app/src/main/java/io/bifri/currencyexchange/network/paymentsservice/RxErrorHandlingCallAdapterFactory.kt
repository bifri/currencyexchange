package io.bifri.currencyexchange.network.paymentsservice

import io.reactivex.*
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.IOException
import java.lang.reflect.Type

class RxErrorHandlingCallAdapterFactory private constructor(scheduler: Scheduler? = null)
    : CallAdapter.Factory() {

    private val original: RxJava2CallAdapterFactory =
            if (scheduler == null) { RxJava2CallAdapterFactory.create() }
            else { RxJava2CallAdapterFactory.createWithScheduler(scheduler) }

    @Suppress("UNCHECKED_CAST")
    override fun get(
            returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit
    ): CallAdapter<Any, Any>? = original.get(returnType, annotations, retrofit)?.let {
        RxCallAdapterWrapper(retrofit, it as CallAdapter<Any, Any>)
    }

    companion object Factory {
        fun create(scheduler: Scheduler? = null): CallAdapter.Factory =
                RxErrorHandlingCallAdapterFactory(scheduler)
    }

}

private class RxCallAdapterWrapper (
        private val retrofit: Retrofit,
        private val wrapped: CallAdapter<Any, Any>
) : CallAdapter<Any, Any> {

    override fun responseType(): Type = wrapped.responseType()

    override fun adapt(call: Call<Any>): Any =
            with(wrapped.adapt(call)) {
                when (this) {
                    is Flowable<*> -> onErrorResumeNext { e: Throwable ->
                        Flowable.error(asRetrofitException(e))
                    }
                    is Single<*> -> onErrorResumeNext { e: Throwable ->
                        Single.error(asRetrofitException(e))
                    }
                    is Maybe<*> -> onErrorResumeNext { e: Throwable ->
                        Maybe.error(asRetrofitException(e))
                    }
                    is Completable -> onErrorResumeNext { e: Throwable ->
                        Completable.error(asRetrofitException(e))
                    }
                    is Observable<*> -> onErrorResumeNext { e: Throwable ->
                        Observable.error(asRetrofitException(e))
                    }
                    else -> this
                }
            }

    private fun asRetrofitException(throwable: Throwable): RetrofitException =
            when (throwable) {
                // We had non-200 http error
                is HttpException -> {
                    val response = throwable.response()
                    RetrofitException.httpError(
                            response?.raw()?.request?.url?.toString().orEmpty(),
                            response,
                            retrofit
                    )
                }
                // A network error happened
                is IOException -> RetrofitException.networkError(throwable)
                // We don't know what happened. We need to simply convert to an unknown error
                else -> RetrofitException.unexpectedError(throwable)
            }

}
