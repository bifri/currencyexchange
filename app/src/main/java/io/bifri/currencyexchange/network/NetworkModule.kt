package io.bifri.currencyexchange.network

import dagger.Module
import io.bifri.currencyexchange.network.paymentsservice.ExchangeRatesServiceModule


@Module(includes = [ExchangeRatesServiceModule::class])
class NetworkModule