package io.bifri.currencyexchange.network.processor

import io.bifri.currencyexchange.exception.ApiException

sealed class Result<out T>

class Loading<T> : Result<T>()

class LoadingError<T>(val error: ApiException) : Result<T>()

class Loaded<T>(val value: T) : Result<T>()