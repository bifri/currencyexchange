package io.bifri.currencyexchange.network.paymentsservice

import retrofit2.Retrofit


class ExchangeRatesApi(retrofit: Retrofit) {

    private val service: ExchangeRatesService = retrofit.create(ExchangeRatesService::class.java)

    val latestRates = service::latestRates

}