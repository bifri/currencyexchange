package io.bifri.currencyexchange.network.paymentsservice

import okhttp3.OkHttpClient
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit

private const val API_BASE_URL = "https://api.exchangeratesapi.io/"

class RetrofitGetter(
        private val client: OkHttpClient,
        private val callAdapterFactory: CallAdapter.Factory,
        private val converterFactories: Set<Converter.Factory>
) {

    val retrofit: Retrofit get() = Retrofit.Builder()
            .addCallAdapterFactory(callAdapterFactory)
            .apply {
                converterFactories.forEach { addConverterFactory(it) }
            }
            .baseUrl(API_BASE_URL)
            .client(client)
            .build()

}