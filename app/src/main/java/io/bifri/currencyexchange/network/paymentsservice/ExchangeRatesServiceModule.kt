package io.bifri.currencyexchange.network.paymentsservice

import com.fasterxml.jackson.databind.ObjectMapper
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoSet
import io.bifri.currencyexchange.di.LOGGING_INTERCEPTOR
import io.bifri.currencyexchange.di.PerApp
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Named


@Module class ExchangeRatesServiceModule {

    @Provides @PerApp fun provideExchangeRatesApi(
            retrofit: Retrofit
    ): ExchangeRatesApi = ExchangeRatesApi(retrofit)

    @Provides @PerApp fun provideRetrofit(
            client: OkHttpClient,
            callAdapterFactory: CallAdapter.Factory,
            converterFactories: Set<@JvmSuppressWildcards Converter.Factory>
    ): Retrofit = RetrofitGetter(
            client,
            callAdapterFactory,
            converterFactories
    )
            .retrofit

    @Provides @PerApp fun provideRetrofitCallAdapterFactory(): CallAdapter.Factory =
            RxErrorHandlingCallAdapterFactory.create()

    @Provides @IntoSet @PerApp fun provideRetrofitScalarsConverterFactory(): Converter.Factory =
            ScalarsConverterFactory.create()

    @Provides @IntoSet @PerApp fun provideRetrofitJacksonConverterFactory(
            objectMapper: ObjectMapper
    ): Converter.Factory = JacksonConverterFactory.create(objectMapper)

    @Provides @PerApp fun provideOkHttpClient(
            interceptors: Collection<Interceptor>
    ): OkHttpClient = OkHttpClientGetter(interceptors).okHttpClient

    @Provides @PerApp fun provideInterceptors(
            @Named(LOGGING_INTERCEPTOR) loggingInterceptor: Interceptor
    ): Collection<@JvmWildcard Interceptor> = listOf(loggingInterceptor)

    @Provides @PerApp @Named(LOGGING_INTERCEPTOR)
    fun provideLoggingInterceptor(): Interceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

}