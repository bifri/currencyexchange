package io.bifri.currencyexchange.network.processor

import io.bifri.currencyexchange.exception.ApiException
import io.bifri.currencyexchange.exception.ApiExceptionStore
import io.bifri.currencyexchange.exception.ExceptionService

class ApiExceptionProcessor(
        private val exceptionService: ExceptionService,
        private val apiExceptionStore: ApiExceptionStore
) {

    fun processApiException(throwable: Throwable) {
        apiExceptionStore.publish(ApiException(throwable))
        exceptionService.processException(throwable)
    }

}