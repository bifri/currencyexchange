package io.bifri.currencyexchange.widgets

import io.bifri.currencyexchange.util.capitalizeWords
import io.bifri.currencyexchange.util.primaryLocale
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*

fun currencyName(currencyCode: String): String =
        try {
            Currency.getInstance(currencyCode).getDisplayName(primaryLocale).capitalizeWords()
        } catch (e: IllegalArgumentException) {
            currencyCode
        }

fun totalStr(currencyCode: String, total: BigDecimal): String {
    val currentCurrency = Currency.getInstance(currencyCode)
    val numberFormatter = NumberFormat.getCurrencyInstance(primaryLocale).apply {
        currency = currentCurrency
        maximumFractionDigits = currentCurrency.defaultFractionDigits
    }
    return numberFormatter.format(total)
}