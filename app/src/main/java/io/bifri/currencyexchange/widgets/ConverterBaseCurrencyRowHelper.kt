package io.bifri.currencyexchange.widgets

import android.content.Context
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import io.bifri.currencyexchange.glide.GlideApp
import io.bifri.currencyexchange.glide.transformRoundCountryFlag
import io.bifri.currencyexchange.util.setTextIfChanged
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_converter_base_currency_row.*


class ConverterBaseCurrencyRowHelper(
        override val containerView: ConverterBaseCurrencyRow
) : LayoutContainer {

    private val context: Context = containerView.context
    private var model: ConverterBaseCurrencyRowModel? = null

    fun setModel(item: ConverterBaseCurrencyRowModel) {
        if (model == item) return

        with(item) {
            if (iconResId != null) {
                GlideApp
                        .with(imageViewConverterBaseCurrencyRowIcon)
                        .load(iconResId)
                        .transformRoundCountryFlag(context)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(imageViewConverterBaseCurrencyRowIcon)
            } else {
                GlideApp
                        .with(imageViewConverterBaseCurrencyRowIcon)
                        .clear(imageViewConverterBaseCurrencyRowIcon)
                imageViewConverterBaseCurrencyRowIcon.setImageDrawable(null)
            }

            textViewConverterBaseCurrencyRowCurrencyCode.setTextIfChanged(currencyCode)
            textViewConverterBaseCurrencyRowCurrencyName.setTextIfChanged(currencyName)
            textViewConverterBaseCurrencyRowTotal.setTextIfChanged(totalStr)
        }

        model = item
    }

}