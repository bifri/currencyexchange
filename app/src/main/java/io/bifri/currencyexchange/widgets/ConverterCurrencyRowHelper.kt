package io.bifri.currencyexchange.widgets

import android.content.Context
import androidx.core.view.isVisible
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import io.bifri.currencyexchange.glide.GlideApp
import io.bifri.currencyexchange.glide.transformRoundCountryFlag
import io.bifri.currencyexchange.util.setTextIfChanged
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_converter_currency_row.*


class ConverterCurrencyRowHelper(
        override val containerView: ConverterCurrencyRow
) : LayoutContainer {

    private val context: Context = containerView.context
    private var model: ConverterCurrencyRowModel? = null

    fun setModel(item: ConverterCurrencyRowModel) {
        if (model == item) return

        with(item) {
            if (iconResId != null) {
                GlideApp
                        .with(imageViewConverterCurrencyRowIcon)
                        .load(iconResId)
                        .transformRoundCountryFlag(context)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(imageViewConverterCurrencyRowIcon)
            } else {
                GlideApp
                        .with(imageViewConverterCurrencyRowIcon)
                        .clear(imageViewConverterCurrencyRowIcon)
                imageViewConverterCurrencyRowIcon.setImageDrawable(null)
            }

            textViewConverterCurrencyRowCurrencyCode.setTextIfChanged(currencyCode)
            textViewConverterCurrencyRowCurrencyName.setTextIfChanged(currencyName)
            textViewConverterCurrencyRowTotal.setTextIfChanged(totalStr)
            textViewConverterCurrencyRowRate.setTextIfChanged(rateStr)
            progressBarConverter.isVisible = isLoading
        }

        model = item
    }

}