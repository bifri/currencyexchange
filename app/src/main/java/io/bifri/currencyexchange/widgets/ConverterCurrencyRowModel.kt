package io.bifri.currencyexchange.widgets

import androidx.annotation.DrawableRes
import io.bifri.currencyexchange.util.isZero
import java.math.BigDecimal
import java.math.BigDecimal.ONE
import java.math.BigDecimal.ZERO
import java.math.RoundingMode
import java.util.*


data class ConverterCurrencyRowModel(
        val currencyCode: String,
        @DrawableRes val iconResId: Int?,
        private val total: BigDecimal,
        private val rate: BigDecimal,
        private val quoteCurrencyCode: String,
        val isLoading: Boolean = false
) {
    val currencyName: String get() = currencyName(currencyCode)

    val totalStr: String get() = totalStr(currencyCode, total)

    val rateStr: String? get() {
        val quoteCurrency = Currency.getInstance(quoteCurrencyCode)
        val inverseRate =
                if (rate.isZero) ZERO
                else ONE.divide(rate, quoteCurrency.defaultFractionDigits, RoundingMode.HALF_EVEN)
        return "1 $currencyCode = $inverseRate $quoteCurrencyCode"
    }

}