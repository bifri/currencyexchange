package io.bifri.currencyexchange.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.AttrRes
import androidx.cardview.widget.CardView
import io.bifri.currencyexchange.R

class ConverterCurrencyRow : CardView {

    private val helper: ConverterCurrencyRowHelper

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, @AttrRes defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    init {
        radius = context.resources.getDimension(R.dimen.converterCurrencyRowCornerRadius)
        LayoutInflater.from(context).inflate(R.layout.view_converter_currency_row, this, true)
        helper = ConverterCurrencyRowHelper(this)
    }

    val setModel = helper::setModel

}