package io.bifri.currencyexchange.widgets

import androidx.annotation.DrawableRes
import java.math.BigDecimal


data class ConverterBaseCurrencyRowModel(
        val currencyCode: String,
        @DrawableRes val iconResId: Int?,
        private val total: BigDecimal
) {
    val currencyName: String get() = currencyName(currencyCode)

    val totalStr: String get() = totalStr(currencyCode, total)

}