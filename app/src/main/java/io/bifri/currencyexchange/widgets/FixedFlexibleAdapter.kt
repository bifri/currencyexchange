package io.bifri.currencyexchange.widgets

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.IFlexible


/**
 * Fixed version of FlexibleAdapter which do not trigger OnUpdateListener
 * immediately after it was added.
 * Commented line: mUpdateListener.onUpdateEmptyView(mainItemCount)
 */
open class FixedFlexibleAdapter<T : IFlexible<VH>, VH : RecyclerView.ViewHolder> : FlexibleAdapter<T> {

    constructor(items: (List<T>)?) : super(items)

    constructor(items: (List<T>)?, listeners: Any?) : super(items, listeners)

    constructor(items: (List<T>)?, listeners: Any?, stableIds: Boolean) :
            super(items, listeners, stableIds)

    @SuppressLint("MissingSuperCall")
    override fun addListener(listener: Any?): FlexibleAdapter<T> {
        if (listener == null) {
            return this
        }
        if (listener is OnItemClickListener) {
            mItemClickListener = listener
            for (holder in allBoundViewHolders) {
                holder.contentView.setOnClickListener(holder)
            }
        }
        if (listener is OnItemLongClickListener) {
            mItemLongClickListener = listener
            // Restore the event
            for (holder in allBoundViewHolders) {
                holder.contentView.setOnLongClickListener(holder)
            }
        }
        if (listener is OnItemMoveListener) {
            mItemMoveListener = listener
        }
        if (listener is OnItemSwipeListener) {
            mItemSwipeListener = listener
        }
        if (listener is OnDeleteCompleteListener) {
            mDeleteCompleteListener = listener
        }
        if (listener is OnStickyHeaderChangeListener) {
            mStickyHeaderChangeListener = listener
        }
        if (listener is OnUpdateListener) {
            mUpdateListener = listener
//          mUpdateListener.onUpdateEmptyView(mainItemCount)
        }
        if (listener is OnFilterListener) {
            mFilterListener = listener
        }
        return this
    }

}