package io.bifri.currencyexchange.common

import android.content.Context
import androidx.annotation.DrawableRes
import com.neovisionaries.i18n.CountryCode
import com.neovisionaries.i18n.CurrencyCode
import io.bifri.currencyexchange.util.DRAWABLE_RESOURCE_TYPE
import io.bifri.currencyexchange.util.NO_RESOURCE
import io.bifri.currencyexchange.util.primaryLocale

private const val FLAG_DRAWABLE_PREFIX = "flag_"

class FlagResIdResolver(private val appContext: Context) {

    private val currencyToCountryId = mapOf(
            "EUR" to CountryCode.EU,
            "CHF" to CountryCode.CH,
            "USD" to CountryCode.US,
            "GBP" to CountryCode.GB,
            "AUD" to CountryCode.AU,
            "CAD" to CountryCode.CA,
            "PLN" to CountryCode.PL,
            "RUB" to CountryCode.RU,
            "JPY" to CountryCode.JP,
            "CNH" to CountryCode.CN,
            "DKK" to CountryCode.DK,
            "SEK" to CountryCode.SE,
            "SEK" to CountryCode.SE,
            "NOK" to CountryCode.NO,
            "SGD" to CountryCode.SG,
            "HKD" to CountryCode.HK,
            "MXN" to CountryCode.MX,
            "NZD" to CountryCode.NZ,
            "TRY" to CountryCode.TR,
            "ZAR" to CountryCode.ZA,
            "CZK" to CountryCode.CZ,
            "HUF" to CountryCode.HU,
            "ILS" to CountryCode.IL,
            "RON" to CountryCode.RO
    )

    @DrawableRes fun flagResIdFromCurrency(currency: String): Int? {
        val countryCode = currencyToCountryId[currency.toUpperCase(primaryLocale)]
                ?: CurrencyCode.getByCode(currency, false).countryList?.get(0)
        return flagResIdFromCountryId(countryCode?.alpha2?.toLowerCase(primaryLocale))
    }

    private fun flagResIdFromCountryId(countryId: String?) = countryId?.let { countryIdNonNull ->
        appContext.resources.getIdentifier(
                "$FLAG_DRAWABLE_PREFIX${countryIdNonNull.toLowerCase(primaryLocale)}",
                DRAWABLE_RESOURCE_TYPE,
                appContext.packageName
        ).takeIf { it != NO_RESOURCE  }
    }

}