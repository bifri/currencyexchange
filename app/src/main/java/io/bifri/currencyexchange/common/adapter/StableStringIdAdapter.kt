package io.bifri.currencyexchange.common.adapter

import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.items.IFlexible
import io.bifri.currencyexchange.widgets.FixedFlexibleAdapter

open class StableStringIdAdapter<T : IFlexible<VH>, VH : RecyclerView.ViewHolder>
    : FixedFlexibleAdapter<T, VH> {

    protected val stableStringIdHelper: StableStringIdHelper

    constructor(
            stableStringIdHelper: StableStringIdHelper,
            items: (List<T>)?
    ) : super(items) {
        this.stableStringIdHelper = stableStringIdHelper
    }

    constructor(
            stableStringIdHelper: StableStringIdHelper,
            items: (List<T>)?,
            listeners: Any?
    ) : super(items, listeners) {
        this.stableStringIdHelper = stableStringIdHelper
    }

    constructor(
            stableStringIdHelper: StableStringIdHelper,
            items: (List<T>)?,
            listeners: Any?,
            stableIds: Boolean
    ) : super(items, listeners, stableIds) {
        this.stableStringIdHelper = stableStringIdHelper
    }

}