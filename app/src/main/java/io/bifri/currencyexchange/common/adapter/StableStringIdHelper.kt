package io.bifri.currencyexchange.common.adapter

class StableStringIdHelper {

    private val strIdToLongId = hashMapOf<String, Long>()
    private val longIdToStrId = hashMapOf<Long, String>()

    private var idGenerator: Long = 0L

    fun longId(strId: String): Long {
        if (strId !in strIdToLongId) {
            val id = idGenerator++
            strIdToLongId[strId] = id
            longIdToStrId[id] = strId
        }
        return strIdToLongId[strId]!!
    }

    fun strId(longId: Long): String? = longIdToStrId[longId]

}