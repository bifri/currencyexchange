package io.bifri.currencyexchange.util

import android.app.Dialog
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.getSystemService


fun TextView.setTextIfChanged(newText: CharSequence?) {
    if (isTextChanged(newText)) text = newText
}

fun TextView.isTextChanged(newText: CharSequence?) =
        text?.toString() ?: "" != newText?.toString() ?: ""

/**
 * Show keyboard and focus to given EditText.
 * Use this method if target EditText is in Dialog.
 *
 * @param dialog Dialog
 */
fun EditText.showKeyboardInDialog(dialog: Dialog) {
    dialog.window?.let {
        it.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        requestFocus()
    }
}

fun View.hideKeyboard() {
    val imm: InputMethodManager = context.getSystemService() ?: return
    imm.hideSoftInputFromWindow(windowToken, 0)
}