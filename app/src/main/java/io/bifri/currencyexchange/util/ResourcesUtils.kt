package io.bifri.currencyexchange.util

import android.content.Context
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat

const val NO_RESOURCE = 0

@ColorInt fun Context.getColorCompat(@ColorRes colorId: Int): Int =
        ContextCompat.getColor(this, colorId)