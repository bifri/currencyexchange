package io.bifri.currencyexchange.util

import android.util.Log
import io.bifri.currencyexchange.BuildConfig
import kotlin.reflect.KClass
import kotlin.reflect.full.companionObject

/**
 * Convenient wrappers over Android Log.* static methods
 * https://gist.github.com/paolop/0bd59e49b33d18d6089fb1bf5488e212
 * https://stackoverflow.com/questions/34416869/idiomatic-way-of-logging-in-kotlin
 */
fun Any.logv(
        message: String?,
        onlyInDebugMode: Boolean = true,
        enclosingClass: KClass<*>? = null,
        throwable: Throwable? = null,
        lazyMessage: () -> String = { "" }
) = log(message, onlyInDebugMode, enclosingClass, throwable, Log::v, Log::v, lazyMessage)

fun Any.logd(
        message: String?,
        onlyInDebugMode: Boolean = true,
        enclosingClass: KClass<*>? = null,
        throwable: Throwable? = null,
        lazyMessage: () -> String = { "" }
) = log(message, onlyInDebugMode, enclosingClass, throwable, Log::d, Log::d, lazyMessage)

fun Any.logi(
        message: String?,
        onlyInDebugMode: Boolean = true,
        enclosingClass: KClass<*>? = null,
        throwable: Throwable? = null,
        lazyMessage: () -> String = { "" }
) = log(message, onlyInDebugMode, enclosingClass, throwable, Log::i, Log::i, lazyMessage)

fun Any.logw(
        message: String?,
        onlyInDebugMode: Boolean = true,
        enclosingClass: KClass<*>? = null,
        throwable: Throwable? = null,
        lazyMessage: () -> String = { "" }
) = log(message, onlyInDebugMode, enclosingClass, throwable, Log::w, Log::w, lazyMessage)

fun Any.loge(
        message: String?,
        onlyInDebugMode: Boolean = true,
        enclosingClass: KClass<*>? = null,
        throwable: Throwable? = null,
        lazyMessage: () -> String = { "" }
) = log(message, onlyInDebugMode, enclosingClass, throwable, Log::e, Log::e, lazyMessage)

private fun Any.log(
        message: String?,
        onlyInDebugMode: Boolean,
        enclosingClass: KClass<*>?,
        throwable: Throwable?,
        logFun1: (String, String) -> Int,
        logFun2: (String, String, Throwable) -> Int,
        lazyMessage: () -> String
) = log(onlyInDebugMode) {
    val resolvedMessage = message ?: lazyMessage.invoke()
    val tag = addThreadSignature(getClassSimpleName(enclosingClass))
    if (throwable == null) logFun1.invoke(tag, resolvedMessage)
    else logFun2.invoke(tag, resolvedMessage, throwable)
}

private fun log(onlyInDebugMode: Boolean, logger: () -> Unit) {
    when {
        onlyInDebugMode && BuildConfig.DEBUG -> logger()
        !onlyInDebugMode -> logger()
    }
}

/**
 * Utility that returns the name of the class from within it is invoked.
 * It allows to handle invocations from anonymous classes given that the string returned by `T::class.java.simpleName`
 * in this case is an empty string.
 *
 * @throws IllegalArgumentException if `enclosingClass` is `null` and this function is invoked within an anonymous class
 */
private fun Any.getClassSimpleName(enclosingClass: KClass<*>?): String =
        this::class.java.let {
            if (it.simpleName.isNotBlank()) {
                unwrapCompanionClass(it).simpleName
            } else { // Enforce the caller to pass a class to retrieve its simple name
                enclosingClass
                        ?.simpleName
                        ?: throw IllegalArgumentException(
                                "enclosingClass cannot be null when invoked from an anonymous class")
            }
        }

private fun addThreadSignature(tag: String): String {
    return "Thread{${Thread.currentThread().name}}/" + tag
}

/**
 *  Unwrap companion class to enclosing class given a Java Class
 */
private fun unwrapCompanionClass(ofClass: Class<*>) =
        ofClass.enclosingClass?.takeIf { it.kotlin.companionObject?.java == ofClass } ?: ofClass