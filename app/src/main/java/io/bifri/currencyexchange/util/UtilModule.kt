package io.bifri.currencyexchange.util

import android.content.Context
import dagger.Module
import dagger.Provides
import io.bifri.currencyexchange.common.FlagResIdResolver
import io.bifri.currencyexchange.di.APP_CONTEXT
import io.bifri.currencyexchange.di.PerApp
import javax.inject.Named


@Module class UtilModule {

    @Provides @PerApp fun provideFlagResIdResolver(
            @Named(APP_CONTEXT) appContext: Context
    ): FlagResIdResolver = FlagResIdResolver(appContext)

}