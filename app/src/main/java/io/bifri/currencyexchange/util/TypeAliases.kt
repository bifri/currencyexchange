package io.bifri.currencyexchange.util

import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.IFlexible

typealias FlexibleAdapterAlias = FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
