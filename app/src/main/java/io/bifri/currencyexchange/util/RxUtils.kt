package io.bifri.currencyexchange.util

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Lifecycle.Event.*
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single


fun <T : Observable<Opt<E?>>, E> T.pickValue(): Observable<E> =
        filter { it.isPresent }.map { it.value }

fun <T : Single<Opt<E?>>, E> T.pickValue(): Maybe<E> =
        filter { it.isPresent }.map { it.value }

fun <T : Maybe<Opt<E?>>, E> T.pickValue(): Maybe<E> =
        filter { it.isPresent }.map { it.value }

fun Observable<Lifecycle.Event>.filterBetweenOnStartAndOnPauseOnce(): Maybe<Lifecycle.Event> =
        filter {
            when (it) {
                ON_START, ON_RESUME, ON_PAUSE -> true
                else -> false
            }
        }
                .firstElement()