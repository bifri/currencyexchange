package io.bifri.currencyexchange.util

import java.util.*


val primaryLocale: Locale get() = Locale.getDefault()