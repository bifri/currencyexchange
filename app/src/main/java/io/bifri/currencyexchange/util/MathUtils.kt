package io.bifri.currencyexchange.util

import java.math.BigDecimal
import java.math.BigDecimal.ZERO

val BigDecimal?.orZero: BigDecimal get() = this ?: ZERO

val BigDecimal.isZero: Boolean get() = compareTo(ZERO) == 0

val BigDecimal.isNotZero: Boolean get() = !isZero

fun BigDecimal?.isTheSame(other: BigDecimal?): Boolean {
    if (this == null || other == null) {
        return this == other
    }
    return compareTo(other) == 0
}