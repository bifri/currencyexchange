package io.bifri.currencyexchange.util

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jakewharton.rxrelay2.BehaviorRelay
import io.bifri.currencyexchange.app.App
import io.reactivex.Observable


class RxCurrentActivityLifecycleCallbacks(app: App) : Application.ActivityLifecycleCallbacks {

    private val currentActivityInternal: BehaviorRelay<Opt<AppCompatActivity?>> = BehaviorRelay.create()

    val currentActivity: Observable<Opt<AppCompatActivity?>> = currentActivityInternal.hide()

    init {
        app.registerActivityLifecycleCallbacks(this)
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {

    }

    override fun onActivityStarted(activity: Activity) {
        currentActivityInternal.accept((activity as AppCompatActivity).opt)
    }

    override fun onActivityResumed(activity: Activity) {

    }

    override fun onActivityPaused(activity: Activity) {

    }

    override fun onActivityStopped(activity: Activity) {
        val lastStartedActivity = currentActivityInternal.value?.value
        if (activity === lastStartedActivity) {
            currentActivityInternal.accept(Opt.empty())
        }
    }

    override fun onActivityDestroyed(activity: Activity) {

    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {

    }

}