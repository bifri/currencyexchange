package io.bifri.currencyexchange.util

import androidx.lifecycle.Lifecycle.Event
import com.trello.lifecycle2.android.lifecycle.RxLifecycleAndroidLifecycle
import com.trello.rxlifecycle3.LifecycleProvider
import com.trello.rxlifecycle3.LifecycleTransformer
import com.trello.rxlifecycle3.RxLifecycle
import io.reactivex.Observable

/**
 * https://github.com/Reyurnible/kotlin-sample-newsapp/blob/master/app/src/main/kotlin/com/github/reyurnible/news/component/scene/RxLifecycleObserver.kt
 */
class RxLifecycleProvider(
        private val lifecycleEvents: Observable<Event>
) : LifecycleProvider<Event> {

    override fun lifecycle(): Observable<Event> = lifecycleEvents.hide()

    override fun <T : Any?> bindToLifecycle(): LifecycleTransformer<T> =
            RxLifecycleAndroidLifecycle.bindLifecycle(lifecycle())

    override fun <T : Any?> bindUntilEvent(event: Event): LifecycleTransformer<T> =
            RxLifecycle.bindUntilEvent(lifecycle(), event)

}