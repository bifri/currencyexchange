package io.bifri.currencyexchange.util


val Throwable.messageFallback get() = message
        ?: StringBuilder(toString()).apply {
            if (!stackTrace.isNullOrEmpty()) {
                append("\n")
                append(stackTrace[0].toString())
            }
        }
                .toString()