package io.bifri.currencyexchange.util

import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager


fun FragmentActivity.isDialogFragmentShown(dialogFragmentTag: String): Boolean =
        supportFragmentManager.isDialogFragmentShown((dialogFragmentTag))

/**
 * https://stackoverflow.com/questions/21352571/android-how-do-i-check-if-dialogfragment-is-showing
 */
fun FragmentManager.isDialogFragmentShown(dialogFragmentTag: String): Boolean =
        (findFragmentByTag(dialogFragmentTag) as DialogFragment?)
                ?.let {
                    val dialog = it.dialog
                    dialog != null && dialog.isShowing && !it.isRemoving
                }
                ?: false