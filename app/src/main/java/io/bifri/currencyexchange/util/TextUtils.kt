package io.bifri.currencyexchange.util

import android.annotation.SuppressLint

val WHITESPACE_REGEX = "\\s".toRegex()

@SuppressLint("DefaultLocale")
fun String.capitalizeWords() = split(WHITESPACE_REGEX).joinToString(" ") { it.capitalize() }