package io.bifri.currencyexchange.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.Relay
import io.bifri.currencyexchange.R
import io.bifri.currencyexchange.app.SimpleEditTextDialogGetter
import io.bifri.currencyexchange.exception.SimpleErrorDialogGetter
import io.bifri.currencyexchange.ui.dialog.simpleedittext.DEFAULT_NEGATIVE_TEXT_ID
import io.bifri.currencyexchange.ui.dialog.simpleedittext.DEFAULT_POSITIVE_TEXT_ID
import io.bifri.currencyexchange.ui.dialog.simpleedittext.SimpleEditTextDialogListener
import io.bifri.currencyexchange.ui.dialog.simpleedittext.SimpleEditTextFragmentArgs
import io.bifri.currencyexchange.ui.dialog.simpleerror.ErrorFragmentArgs
import io.bifri.currencyexchange.ui.dialog.simpleerror.SimpleErrorDialogListener
import io.bifri.currencyexchange.util.DEFAULT_DIALOG_ID
import io.bifri.currencyexchange.util.hideKeyboard
import io.bifri.currencyexchange.util.isDialogFragmentShown
import io.reactivex.Observable
import javax.inject.Inject

const val FRAGMENT_TAG_SIMPLE_ERROR_DIALOG = "simpleErrorDialog"
const val FRAGMENT_TAG_SIMPLE_EDIT_TEXT_DIALOG = "simpleEditTextDialog"
const val DEFAULT_SIMPLE_ERROR_DIALOG_TITLE = R.string.error
const val DEFAULT_EDIT_TEXT_DIALOG_TITLE = R.string.converterEnterBuyAmount

abstract class BaseFragment<Args, ViewState> :
        InitFragment(),
        SimpleErrorDialogListener,
        SimpleEditTextDialogListener
{
    @Inject lateinit var simpleErrorDialogGetter: SimpleErrorDialogGetter
    @Inject lateinit var simpleEditTextDialogGetter: SimpleEditTextDialogGetter
    private val initArgsIntent: Relay<Args> = BehaviorRelay.create()
    @StringRes open val titleResId: Int = R.string.EMPTY
    protected abstract val args: Args
    protected var viewState: ViewState? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onInitArgsIntent(args)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(layoutId, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setTitle(titleResId)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        viewState = null
    }

    override fun onDestroyView() {
        super.onDestroyView()
        requireActivity().currentFocus?.hideKeyboard()
        (parentFragment as? DialogFragment)?.dialog?.currentFocus?.hideKeyboard()
    }

    protected abstract val layoutId: Int

    private fun setTitle(@StringRes titleResId: Int) =
            (requireActivity() as AppCompatActivity).supportActionBar!!.setTitle(titleResId)

    fun initArgsIntent(): Observable<Args> = initArgsIntent

    private val onInitArgsIntent = initArgsIntent::accept

    protected fun showSimpleErrorDialog(
            dialogId: Int = DEFAULT_DIALOG_ID,
            message: String?,
            title: String = getString(DEFAULT_SIMPLE_ERROR_DIALOG_TITLE)
    ) {
        val dialogTag = simpleErrorDialogFragmentTag(dialogId)
        if (parentFragmentManager.isDialogFragmentShown(dialogTag)) return
        val errorFragmentArgs = ErrorFragmentArgs(dialogId, title, message)
        simpleErrorDialogGetter.newInstance(errorFragmentArgs, this)
                .show(parentFragmentManager, dialogTag)
    }

    protected fun showSimpleEditTextDialog(
            dialogId: Int = DEFAULT_DIALOG_ID,
            title: Int = DEFAULT_EDIT_TEXT_DIALOG_TITLE,
            @StringRes editTextHintResId: Int? = null,
            @StringRes negativeTextId: Int = DEFAULT_NEGATIVE_TEXT_ID,
            @StringRes positiveTextId: Int = DEFAULT_POSITIVE_TEXT_ID,
            @LayoutRes layoutResId: Int = R.layout.dialog_fragment_simple_edit_text
    ) {
        val dialogTag = simpleEditTextDialogFragmentTag(dialogId)
        if (parentFragmentManager.isDialogFragmentShown(dialogTag)) return
        val fragmentArgs = SimpleEditTextFragmentArgs(
                dialogId, title, editTextHintResId, negativeTextId, positiveTextId, layoutResId
        )
        simpleEditTextDialogGetter.newInstance(fragmentArgs, this)
                .show(parentFragmentManager, dialogTag)
    }

    override fun onDismissErrorDialog(dialogId: Int) {}

    override fun onDismissEditTextDialog(dialogId: Int) {}

    override fun onAcceptEditTextDialog(dialogId: Int, text: String?) {}

    private fun simpleErrorDialogFragmentTag(id: Int) = FRAGMENT_TAG_SIMPLE_ERROR_DIALOG + id

    private fun simpleEditTextDialogFragmentTag(id: Int) = FRAGMENT_TAG_SIMPLE_EDIT_TEXT_DIALOG + id

}