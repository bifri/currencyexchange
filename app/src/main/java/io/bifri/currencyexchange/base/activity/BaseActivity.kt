package io.bifri.currencyexchange.base.activity

import android.os.Bundle
import androidx.lifecycle.Lifecycle
import com.trello.lifecycle2.android.lifecycle.AndroidLifecycle
import com.trello.rxlifecycle3.LifecycleProvider
import io.bifri.currencyexchange.app.App
import io.bifri.currencyexchange.base.viewmodel.RxViewBinder
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

abstract class BaseActivity : AppCompatLifecycleLoggingActivity(), HasActivityComponent {

    abstract val viewBinder: RxViewBinder
    private var compositeDisposable: CompositeDisposable? = null
    private lateinit var lifecycleProvider: LifecycleProvider<Lifecycle.Event>
    private var activityComponentInternal: Any? = null
    val lifecycle: Observable<Lifecycle.Event> get() = lifecycleProvider.lifecycle()

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        lifecycleProvider = AndroidLifecycle.createLifecycleProvider(this)
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        viewBinder.bind()
        subscribe()
    }

    override fun onStop() {
        viewBinder.unbind()
        unsubscribe()
        super.onStop()
    }

    open fun subscribeInternal(cd: CompositeDisposable) {}

    override val activityComponent get() = activityComponentInternal

    private fun subscribe() {
        unsubscribe()
        compositeDisposable = CompositeDisposable()
        subscribeInternal(compositeDisposable!!)
    }

    private fun unsubscribe() {
        compositeDisposable?.let {
            it.clear()
            compositeDisposable = null
        }
    }

    private fun inject() {
        check(activityComponentInternal == null) { "Activity must not use Injector more than once" }
        val componentInjector = (application as App).activityComponentInjector(this)
        activityComponentInternal = componentInjector.component
        componentInjector.injector.inject(this)
    }

}