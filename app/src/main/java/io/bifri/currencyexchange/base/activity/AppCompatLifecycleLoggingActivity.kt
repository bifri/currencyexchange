package io.bifri.currencyexchange.base.activity

import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build.VERSION_CODES.LOLLIPOP
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import io.bifri.currencyexchange.util.logi


/**
 * This abstract class extends the Activity class and overrides
 * lifecycle callbacks for logging various lifecycle events.
 */
abstract class AppCompatLifecycleLoggingActivity : AppCompatActivity() {

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(newBase)
        logi( "attachBaseContext()")
    }

    /**
     * Hook method called when a new instance of Activity is created.
     * One time initialization code should go here e.g. UI layout,
     * some class scope variable initialization. If finish() is
     * called from onCreate no other life cycle callback is called
     * except for onDestroy().
     *
     * @param savedInstanceState object that contains saved state information.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        // Always call super class for necessary
        // initialization/implementation.
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null) {
            // The activity is being re-created. Use the
            // savedInstanceState bundle for initializations either
            // during onCreate or onRestoreInstanceState().
            logi("onCreate(): activity re-created from savedInstanceState")

        } else {
            // Activity is being created anew. No prior saved
            // instance state information available in Bundle object.
            logi("onCreate(): activity created anew")
        }
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)
        logi("onAttachFragment()")
    }

    override fun onContentChanged() {
        super.onContentChanged()
        logi("onContentChanged()")
    }

    /**
     * Hook method called after onCreate() or after onRestart() (when
     * the activity is being restarted from stopped state). Should
     * re-acquire resources relinquished when activity was stopped
     * (onStop()) or acquire those resources for the first time after
     * onCreate().
     */
    override fun onStart() {
        // Always call super class for necessary
        // initialization/implementation.
        super.onStart()
        logi("onStart()")
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        logi("onRestoreInstanceState(Bundle?)")
    }

    @TargetApi(LOLLIPOP)
    override fun onRestoreInstanceState(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onRestoreInstanceState(savedInstanceState, persistentState)
        logi("onRestoreInstanceState(Bundle?, PersistableBundle?)")
    }

    override fun onPostCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onPostCreate(savedInstanceState, persistentState)
        logi("onPostCreate()")
    }

    /**
     * Hook method called after onRestoreStateInstance(Bundle) only if
     * there is a prior saved instance state in Bundle object.
     * onResume() is called immediately after onStart(). onResume()
     * is called when user resumes activity from paused state
     * (onPause()) User can begin interacting with activity.  Place to
     * start animations, acquire exclusive resources, such as the
     * camera.
     */
    override fun onResume() {
        // Always call super class for necessary
        // initialization/implementation and then log which lifecycle
        // hook method is being called.
        super.onResume()
        logi("onResume()")
    }

    override fun onPostResume() {
        super.onPostResume()
        logi("onPostResume()")
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        logi("onAttachedToWindow()")
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        logi("onCreateOptionsMenu()")
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        logi("onPrepareOptionsMenu()")
        return super.onPrepareOptionsMenu(menu)
    }

    /**
     * Hook method called when an Activity loses focus but is still
     * visible in background. May be followed by onStop() or
     * onResume(). Delegate more CPU intensive operation to onStop
     * for seamless transition to next activity. Save persistent
     * state (onSaveInstanceState()) in case app is killed. Often
     * used to release exclusive resources.
     */
    override fun onPause() {
        // Always call super class for necessary
        // initialization/implementation and then log which lifecycle
        // hook method is being called.
        super.onPause()
        logi("onPause()")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        logi("onSaveInstanceState()")
    }

    @TargetApi(LOLLIPOP)
    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        logi("onSaveInstanceState()")
    }

    /**
     * Called when Activity is no longer visible. Release resources
     * that may cause memory leak. Save instance state
     * (onSaveInstanceState()) in case activity is killed.
     */
    override fun onStop() {
        // Always call super class for necessary
        // initialization/implementation and then log which lifecycle
        // hook method is being called.
        super.onStop()
        logi("onStop()")
    }

    /**
     * Hook method called when user restarts a stopped activity. Is
     * followed by a call to onStart() and onResume().
     */
    override fun onRestart() {
        // Always call super class for necessary
        // initialization/implementation and then log which lifecycle
        // hook method is being called.
        super.onRestart()
        logi("onRestart()")
    }

    /**
     * Hook method that gives a final chance to release resources and
     * stop spawned threads. onDestroy() may not always be
     * called-when system kills hosting process
     */
    override fun onDestroy() {
        // Always call super class for necessary
        // initialization/implementation and then log which lifecycle
        // hook method is being called.
        super.onDestroy()
        logi("onDestroy()")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        logi("onActivityResult()")
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        logi("onConfigurationChanged()")
    }

}
