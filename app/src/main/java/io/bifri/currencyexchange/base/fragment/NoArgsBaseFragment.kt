package io.bifri.currencyexchange.base.fragment

abstract class NoArgsBaseFragment<ViewState> : BaseFragment<Unit, ViewState>() {

    override val args = Unit

}