package io.bifri.currencyexchange.base.viewmodel

import io.bifri.currencyexchange.exception.ExceptionService

abstract class BaseRxViewBinder(val exceptionService: ExceptionService) : RxViewBinder()