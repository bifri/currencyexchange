package io.bifri.currencyexchange.base.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.Lifecycle.Event
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.Relay
import com.trello.rxlifecycle3.LifecycleProvider
import io.bifri.currencyexchange.util.RxLifecycleProvider
import io.bifri.currencyexchange.util.logv
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel(application: Application) : AndroidViewModel(application) {

    private var compositeDisposable: CompositeDisposable? = null
    private val lifecycleEvents: Relay<Event> = BehaviorRelay.create()
    protected val lifecycleProvider: LifecycleProvider<Event> = RxLifecycleProvider(lifecycleEvents)

    override fun onCleared() {
        unsubscribeFromDataStore()
    }

    fun subscribeToDataStore() {
        logv("subscribeToDataStore")

        if (compositeDisposable == null) {
            compositeDisposable = CompositeDisposable()
            subscribeToDataStoreInternal(compositeDisposable!!)
        }
    }

    abstract fun subscribeToDataStoreInternal(compositeDisposable: CompositeDisposable)

    private fun unsubscribeFromDataStore() {
        logv("unsubscribeToDataStore")

        compositeDisposable?.let {
            it.clear()
            compositeDisposable = null
        }
    }

    val onLifecycleEvent = lifecycleEvents::accept

}
