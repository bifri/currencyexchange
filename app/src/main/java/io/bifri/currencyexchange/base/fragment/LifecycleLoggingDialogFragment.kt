package io.bifri.currencyexchange.base.fragment

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import io.bifri.currencyexchange.util.logi

abstract class LifecycleLoggingDialogFragment : DialogFragment() {

    override fun onAttach(context: Context) {
        super.onAttach(context)
        logi( "onAttach()")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        logi("onCreate()")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        logi( "onCreateDialog()")
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        logi( "onCreateView()")
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        logi( "onViewCreated()")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        logi("onActivityCreated()")
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        logi("onViewStateRestored()")
    }

    override fun onStart() {
        super.onStart()
        logi("onStart()")
    }

    override fun onResume() {
        super.onResume()
        logi("onResume()")
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        logi("onCreateOptionsMenu()")
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        logi("onPrepareOptionsMenu()")
    }

    override fun onPause() {
        super.onPause()
        logi("onPause()")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        logi("onSaveInstanceState()")
    }

    override fun onStop() {
        super.onStop()
        logi("onStop()")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        logi("onDestroyView()")
    }

    override fun onDestroy() {
        super.onDestroy()
        logi("onDestroy()")
    }

    override fun onDetach() {
        super.onDetach()
        logi("onDetach()")
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        logi("onHiddenChanged($hidden)")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        logi("onActivityResult()")
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        logi("onConfigurationChanged()")
    }

    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        logi("onAttachFragment()")
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        logi("onCancel()")
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        logi("onDismiss()")
    }

}