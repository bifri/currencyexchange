package io.bifri.currencyexchange.base.activity

interface HasActivityComponent {
    val activityComponent: Any?
}