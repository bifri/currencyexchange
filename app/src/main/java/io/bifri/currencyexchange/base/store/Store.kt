package io.bifri.currencyexchange.base.store

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.bifri.currencyexchange.util.Opt
import io.bifri.currencyexchange.util.opt
import io.reactivex.Completable
import io.reactivex.Observable

abstract class Store<T>(
        getInitialValue: (() -> T?)? = null,
        emitRecent: Boolean = true,
        private val distinctUntilChanged: Boolean = true
) {

    protected open val storeSubject: Relay<Lazy<T?>> =
            if (!emitRecent) {
                @Suppress("RemoveExplicitTypeArguments")
                PublishRelay.create<Lazy<T?>>()
            } else {
                (getInitialValue
                        ?.let { BehaviorRelay.createDefault(lazy { it.invoke() }) }
                        ?: BehaviorRelay.create()
                        )
            }
                    .toSerialized()

    /**
     * Initial value is calculated once on a subscription thread
     */
    fun observe(): Observable<Opt<T?>> = storeSubject
            .hide()
            .map { it.value.opt }
            .compose {
                if (distinctUntilChanged) it.distinctUntilChanged() else it
            }

    fun publish(value: T?) = storeSubject.accept(lazyOf(value))

    fun clear() = publish(null)

    val clearCompletable get() = Completable.fromCallable(::clear)

}