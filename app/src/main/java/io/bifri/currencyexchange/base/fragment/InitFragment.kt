package io.bifri.currencyexchange.base.fragment

import android.content.Context
import androidx.lifecycle.Lifecycle.Event
import com.trello.lifecycle2.android.lifecycle.AndroidLifecycle
import com.trello.rxlifecycle3.LifecycleProvider
import io.bifri.currencyexchange.app.App
import io.bifri.currencyexchange.base.activity.HasActivityComponent
import io.bifri.currencyexchange.base.viewmodel.RxViewBinder
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

abstract class InitFragment : LifecycleLoggingFragment() {

    abstract val viewBinder: RxViewBinder
    private var isInjectorUsed: Boolean = false
    private var compositeDisposable: CompositeDisposable? = null
    private lateinit var lifecycleProvider: LifecycleProvider<Event>
    val lifecycle: Observable<Event> get() = lifecycleProvider.lifecycle()

    override fun onAttach(context: Context) {
        inject()
        lifecycleProvider = AndroidLifecycle.createLifecycleProvider(this)
        super.onAttach(context)
    }

    override fun onStart() {
        super.onStart()
        viewBinder.bind()
        subscribe()
    }

    override fun onStop() {
        viewBinder.unbind()
        unsubscribe()
        super.onStop()
    }

    open fun subscribeInternal(cd: CompositeDisposable) {}

    private fun subscribe() {
        unsubscribe()
        compositeDisposable = CompositeDisposable()
        subscribeInternal(compositeDisposable!!)
    }

    private fun unsubscribe() {
        compositeDisposable?.let {
            it.clear()
            compositeDisposable = null
        }
    }

    private fun inject() {
        if (isInjectorUsed) return
        (requireActivity().application as App).fragmentInjector(
                this, (activity as HasActivityComponent).activityComponent!!
        )
                .inject(this)
        isInjectorUsed = true
    }

}