package io.bifri.currencyexchange.base.viewmodel

import io.reactivex.disposables.CompositeDisposable

abstract class RxViewBinder {

    private var compositeDisposable: CompositeDisposable? = null

    fun bind() {
        unbind()
        compositeDisposable = CompositeDisposable()
        bindInternal(compositeDisposable!!)
    }

    fun unbind() {
        compositeDisposable?.let {
            it.clear()
            compositeDisposable = null
        }
    }

    protected abstract fun bindInternal(cd: CompositeDisposable)
}