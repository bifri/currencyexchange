package io.bifri.currencyexchange.glide

import android.content.Context
import com.bumptech.glide.request.RequestOptions
import io.bifri.currencyexchange.R
import io.bifri.currencyexchange.util.getColorCompat
import jp.wasabeef.glide.transformations.CropCircleWithBorderTransformation

fun <T> GlideRequest<T>.transformRoundCountryFlag(context: Context): GlideRequest<T> {
    val borderColor = context.getColorCompat(R.color.blackOpacity10)
    val options = RequestOptions()
            .transform(CropCircleWithBorderTransformation(1, borderColor))
    return apply(options)
}