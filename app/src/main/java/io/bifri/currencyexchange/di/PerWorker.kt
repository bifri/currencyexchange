package io.bifri.currencyexchange.di


import javax.inject.Scope

/**
 * Custom scope for Android Workers singletons
 */
@Scope annotation class PerWorker