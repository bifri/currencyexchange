package io.bifri.currencyexchange.di

interface Injector {
    fun inject(target: Any)
}