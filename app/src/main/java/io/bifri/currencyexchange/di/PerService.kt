package io.bifri.currencyexchange.di


import javax.inject.Scope

/**
 * Custom scope for Android Service singletons
 */
@Scope annotation class PerService