package io.bifri.currencyexchange.di

const val APP_CONTEXT = "appContext"
const val ACTIVITY_CONTEXT = "activityContext"
const val APP_INJECTOR = "appInjector"
const val LOGGING_INTERCEPTOR = "loggingInterceptor"
const val VERTICAL_LAYOUT_MANAGER = "verticalLayoutManager"