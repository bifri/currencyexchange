package io.bifri.currencyexchange.di


import javax.inject.Scope

/**
 * Custom scope for global application singletons
 */
@Scope annotation class PerApp
