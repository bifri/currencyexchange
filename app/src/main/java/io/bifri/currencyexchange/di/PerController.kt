package io.bifri.currencyexchange.di


import javax.inject.Scope

/**
 * Custom scope for controller (activity, fragment) singletons
 */
@Scope annotation class PerController