package io.bifri.currencyexchange.di

import java.lang.reflect.Method
import java.util.concurrent.ConcurrentHashMap

/**
 * https://github.com/konmik/nucleus/blob/master/nucleus-example-real-life/src/main/java/nucleus/example/util/ComponentReflectionInjector.java
 *
 * This class allows to inject into objects through a base class,
 * so we don't have to repeat injection code everywhere.
 *
 * The performance drawback is about 0.013 ms per injection on a very slow device,
 * which is negligible in most cases.
 *
 * Example:
 * <pre>{@code
 * Component {
 *     void inject(B b);
 * }
 *
 * class A {
 *     void onCreate() {
 *         componentReflectionInjector.inject(this);
 *     }
 * }
 *
 * class B extends A {
 *     @Inject MyDependency dependency;
 * }
 *
 * new B().onCreate() // dependency will be injected at this point
 *
 * class C extends B {
 *
 * }
 *
 * new C().onCreate() // dependency will be injected at this point as well
 * }</pre>
 *
 * @param <T> a type of dagger 2 component.
 */
class ComponentReflectionInjector<T>(
        private val componentClass: Class<T>,
        private val component: T
) : Injector {
    private val methods: Map<Class<*>, Method>

    init {
        methods = getMethods(componentClass)
    }

    override fun inject(target: Any) {
        var targetClass: Class<*>? = target.javaClass
        var method: Method? = targetClass?.let { methods[it] }
        while (method == null && targetClass != null) {
            targetClass = targetClass.superclass
            method = targetClass?.let { methods[it] }
        }
        if (method == null) {
            throw RuntimeException("No ${target.javaClass} injecting method exists " +
                    "in $componentClass component")
        }
        try { method.invoke(component, target) }
        catch (e: Exception) { throw RuntimeException(e) }
    }

    companion object {
        private val cache = ConcurrentHashMap<Class<*>, MutableMap<Class<*>, Method>>()

        private fun getMethods(componentClass: Class<*>): Map<Class<*>, Method> {
            cache[componentClass]?.let { return it }
            synchronized(cache) {
                cache[componentClass]?.let { return it }
                val methods: MutableMap<Class<*>, Method> = hashMapOf()
                for (method in componentClass.methods) {
                    val params = method.parameterTypes
                    if (params.size == 1) {
                        methods[params[0]] = method
                    }
                }
                cache[componentClass] = methods
                return methods
            }
        }

    }

}
