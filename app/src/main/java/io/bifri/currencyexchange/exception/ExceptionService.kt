package io.bifri.currencyexchange.exception

interface ExceptionService {
    fun processException(t: Throwable)
}
