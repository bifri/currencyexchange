package io.bifri.currencyexchange.exception

import io.bifri.currencyexchange.base.store.Store

class ApiExceptionStore : Store<ApiException>(emitRecent = false, distinctUntilChanged = false)