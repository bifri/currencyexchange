package io.bifri.currencyexchange.exception

class UncaughtExceptionHandlerGetter(private val exceptionService: ExceptionService) {

    val uncaughtExceptionHandler: Thread.UncaughtExceptionHandler get() =
        Thread.getDefaultUncaughtExceptionHandler().let { defaultHandler ->
            Thread.UncaughtExceptionHandler { thread, e ->
                exceptionService.processException(e)
                defaultHandler?.uncaughtException(thread, e)
            }
        }

}