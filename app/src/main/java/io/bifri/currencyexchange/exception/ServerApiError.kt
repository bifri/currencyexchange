package io.bifri.currencyexchange.exception

class ApiException(val throwable: Throwable)