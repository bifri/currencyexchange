package io.bifri.currencyexchange.exception

import io.bifri.currencyexchange.util.loge

class DefaultExceptionService : ExceptionService {

    override fun processException(t: Throwable) {
        loge(message = t.message, throwable = t)
    }

}