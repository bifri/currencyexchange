package io.bifri.currencyexchange.exception

import androidx.fragment.app.FragmentActivity
import io.bifri.currencyexchange.base.fragment.DEFAULT_SIMPLE_ERROR_DIALOG_TITLE
import io.bifri.currencyexchange.ui.dialog.simpleerror.ErrorFragmentArgs
import io.bifri.currencyexchange.ui.dialog.simpleerror.SimpleErrorMessage
import io.bifri.currencyexchange.ui.dialog.simpleerror.SimpleErrorMessageConverter
import io.bifri.currencyexchange.util.RxCurrentActivityLifecycleCallbacks
import io.bifri.currencyexchange.util.isDialogFragmentShown
import io.bifri.currencyexchange.util.pickValue
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.rxkotlin.subscribeBy

const val FRAGMENT_TAG_SIMPLE_ERROR_DIALOG = "simpleErrorDialog#"

class SimpleApiErrorAndExceptionHandler(
        private val rxCurrentActivityLifecycleCallbacks: RxCurrentActivityLifecycleCallbacks,
        private val exceptionService: ExceptionService,
        private val apiExceptionStore: ApiExceptionStore,
        private val errorConverter: SimpleErrorMessageConverter,
        private val simpleErrorDialogGetter: SimpleErrorDialogGetter
) {

    fun register() = apiExceptionStore.observe().pickValue()
            .observeOn(mainThread())
            .switchMapMaybe { apiError ->
                rxCurrentActivityLifecycleCallbacks.currentActivity
                        .firstElement()
                        .filter { it.isPresent }
                        .map {
                            val message = errorConverter.from(apiError.throwable)
                            it.value!! to message
                        }
            }
            .subscribeBy(
                    onNext = { (activity, message) ->
                        showSimpleErrorDialog(activity, message)
                    },
                    onError = exceptionService::processException
            )

    private fun showSimpleErrorDialog(
            activity: FragmentActivity,
            simpleErrorMessage: SimpleErrorMessage
    ) {
        val dialogId = simpleErrorMessage.hashCode()
        val dialogTag = simpleErrorDialogFragmentTag(dialogId)
        if (activity.isDialogFragmentShown(dialogTag)) return
        val errorFragmentArgs = ErrorFragmentArgs(
                dialogId,
                activity.getString(DEFAULT_SIMPLE_ERROR_DIALOG_TITLE),
                simpleErrorMessage.message
        )
        simpleErrorDialogGetter.newInstance(errorFragmentArgs).show(
                activity.supportFragmentManager,
                dialogTag
        )
    }

    private fun simpleErrorDialogFragmentTag(id: Int) = FRAGMENT_TAG_SIMPLE_ERROR_DIALOG + id

}