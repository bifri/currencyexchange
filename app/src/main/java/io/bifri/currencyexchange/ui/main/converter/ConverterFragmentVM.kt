package io.bifri.currencyexchange.ui.main.converter

import android.app.Application
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.bifri.currencyexchange.base.viewmodel.BaseViewModel
import io.bifri.currencyexchange.exception.ExceptionService
import io.bifri.currencyexchange.network.processor.Loaded
import io.bifri.currencyexchange.network.processor.SimpleProcessor
import io.bifri.currencyexchange.util.Event
import io.bifri.currencyexchange.util.filterBetweenOnStartAndOnPauseOnce
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.Observables.combineLatest
import io.reactivex.rxkotlin.subscribeBy
import java.math.BigDecimal
import java.math.BigDecimal.ONE
import java.util.concurrent.TimeUnit

private const val RATES_UPDATE_INTERVAL_IN_SECONDS = 1L

class ConverterFragmentVM(
        application: Application,
        private val exceptionService: ExceptionService,
        private val simpleProcessor: SimpleProcessor,
        private val converterHelper: ConverterHelper
) : BaseViewModel(application) {

    private val onAmountChangeIntent: Relay<BigDecimal> = PublishRelay.create()
    private val onBaseCurrencyClickIntent: Relay<Unit> = PublishRelay.create()
    private val onCurrencyClickIntent: Relay<String> = PublishRelay.create()
    private val onNavigateDoneIntent: Relay<Unit> = PublishRelay.create()
    private val converterState: Relay<ConverterState> = BehaviorRelay.create()

    init {
        subscribeToDataStore()
    }

    val onAmountChange = onAmountChangeIntent::accept
    val onBaseCurrencyClick = onBaseCurrencyClickIntent::accept
    val onCurrencyClick = onCurrencyClickIntent::accept
    val onNavigateDone = onNavigateDoneIntent::accept

    fun converterState(): Observable<ConverterState> = converterState

    override fun subscribeToDataStoreInternal(compositeDisposable: CompositeDisposable) {
        val amountChange = onAmountChangeIntent
                .startWith(ONE)
                .replay(1)

        val currencies = onCurrencyClickIntent
                .startWith(converterHelper.startCurrency)
                .switchMap { baseCurrency -> combineLatest(
                        amountChange,
                        simpleProcessor.loadData { latestRates(baseCurrency) }
                                .filter { it is Loaded }
                                .map { (it as Loaded).value }
                                .repeatWhen { source ->
                                    source.delay(
                                            RATES_UPDATE_INTERVAL_IN_SECONDS,
                                            TimeUnit.SECONDS
                                    )
                                            .flatMapMaybe {
                                                lifecycleProvider.lifecycle().filterBetweenOnStartAndOnPauseOnce()
                                            }
                                }
                ) { amount, currencies ->
                    @Suppress("USELESS_CAST")
                    LoadedCurrencies(baseCurrency, amount, currencies) as PartialState
                }
                        .startWith(ChangingBaseCurrency(baseCurrency))
                }

        val showEnterAmount = onBaseCurrencyClickIntent
                .map { Navigate(EnterAmountDirection) }

        val navigateDone = onNavigateDoneIntent.map { NavigateDone }

        val allIntentsObservable = Observable.mergeArray(
                currencies, showEnterAmount, navigateDone
        )
        val initialState = ConverterState()

        compositeDisposable.addAll(
                allIntentsObservable
                        .scan(initialState) { previousState, changes ->
                            viewStateReducer(previousState, changes)
                        }
                        .distinctUntilChanged()
                        .observeOn(mainThread())
                        .subscribeBy(
                                onNext = converterState::accept,
                                onError = exceptionService::processException
                        ),
                amountChange.connect()
        )
    }

    private fun viewStateReducer(
            previousState: ConverterState,
            changes: PartialState
    ): ConverterState = when (changes) {
        is ChangingBaseCurrency -> converterHelper.changingBaseCurrencyToConverterState(
                previousState, changes
        )
        is LoadedCurrencies -> converterHelper.loadedCurrenciesToConverterState(
                previousState, changes
        )
        is Navigate -> previousState.copy(navigateEvent = Event(changes.direction))
        NavigateDone -> previousState.copy(navigateEvent = null)
    }

}