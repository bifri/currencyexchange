package io.bifri.currencyexchange.ui.main

import dagger.BindsInstance
import dagger.Subcomponent
import io.bifri.currencyexchange.ui.main.converter.ConverterFragmentComponent

@PerMainActivity
@Subcomponent(modules = [
    CommonModule::class,
    MainActivityModule::class
])
interface MainActivityComponent {

    @Subcomponent.Builder interface Builder {
        @BindsInstance fun mainActivity(activity: MainActivity): Builder

        fun build(): MainActivityComponent
    }

    fun newConverterFragmentComponentBuilder(): ConverterFragmentComponent.Builder

    fun inject(mainActivity: MainActivity)

}