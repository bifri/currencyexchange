package io.bifri.currencyexchange.ui.dialog.simpleerror

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import io.bifri.currencyexchange.util.DEFAULT_DIALOG_ID
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import io.bifri.currencyexchange.base.fragment.FixedDialogFragment


data class ErrorFragmentArgs(
        val dialogId: Int = DEFAULT_DIALOG_ID,
        val title: String? = null,
        val message: String? = null
)

class SimpleErrorDialogFragment : FixedDialogFragment() {

    private lateinit var args: ErrorFragmentArgs

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        args = args(arguments)
        val alertDialogBuilder = MaterialAlertDialogBuilder(requireContext())
                .setTitle(args.title)
                .setMessage(args.message)
                .setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }
        return alertDialogBuilder.create()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        listener?.onDismissErrorDialog(args.dialogId)
    }

    private val listener: SimpleErrorDialogListener?
        get() = targetFragment as? SimpleErrorDialogListener

    companion object {
        private const val KEY_DIALOG_ID = "dialogId"
        private const val KEY_TITLE = "title"
        private const val KEY_MESSAGE = "message"

        fun newInstance(
                fragmentArgs: ErrorFragmentArgs,
                targetFragment: Fragment? = null,
                requestCode: Int = 0
        ): SimpleErrorDialogFragment =
                SimpleErrorDialogFragment().apply {
                    arguments = with(fragmentArgs) { bundleOf(
                            KEY_DIALOG_ID to dialogId,
                            KEY_TITLE to title,
                            KEY_MESSAGE to message
                    )}
                    targetFragment?.let { setTargetFragment(it, requestCode) }
                }

        private fun args(arguments: Bundle?): ErrorFragmentArgs = arguments
                ?.let {
                    with(it) {
                        ErrorFragmentArgs(
                                getInt(KEY_DIALOG_ID),
                                getString(KEY_TITLE),
                                getString(KEY_MESSAGE)
                        )
                    }
                }
                ?: ErrorFragmentArgs()
    }

}

interface SimpleErrorDialogListener {
    fun onDismissErrorDialog(dialogId: Int)
}