package io.bifri.currencyexchange.ui.main.converter

import android.view.View
import dagger.Module
import dagger.Provides
import io.bifri.currencyexchange.app.StableStringIdGetter
import io.bifri.currencyexchange.util.FlexibleAdapterAlias

@Module class ConverterRecyclerViewModule {

    @Provides @PerConverterFragment fun provideConverterCurrenciesAdapterGetter(
            stableStringIdGetter: StableStringIdGetter
    ): ConverterCurrenciesAdapterGetter = object : ConverterCurrenciesAdapterGetter {
        override val converterCurrenciesAdapter: ConverterCurrenciesAdapter
            get() = ConverterCurrenciesAdapter(
                    stableStringIdGetter.stableStringIdHelper,
                    emptyList(),
                    null,
                    true
            )
    }

    @Provides @PerConverterFragment
    fun provideConverterCurrenciesAdapterBaseCurrencyItemGetter(
            viewHolderGetter: ConverterCurrenciesAdapterBaseCurrencyViewHolderGetter
    ): ConverterCurrenciesAdapterBaseCurrencyItemGetter = object : ConverterCurrenciesAdapterBaseCurrencyItemGetter {
        override fun invoke(
                itemModel: ConverterCurrenciesAdapterBaseCurrencyItemModel
        ): ConverterCurrenciesAdapterBaseCurrencyItem = ConverterCurrenciesAdapterBaseCurrencyItem(
                viewHolderGetter, itemModel
        )
    }

    @Provides @PerConverterFragment
    fun provideConverterCurrenciesAdapterBaseCurrencyViewHolderGetter(): ConverterCurrenciesAdapterBaseCurrencyViewHolderGetter =
            object : ConverterCurrenciesAdapterBaseCurrencyViewHolderGetter {
                override fun invoke(
                        view: View, adapter: FlexibleAdapterAlias
                ): ConverterCurrenciesAdapterBaseCurrencyViewHolder = ConverterCurrenciesAdapterBaseCurrencyViewHolder(
                        view, adapter
                )
            }

    @Provides @PerConverterFragment fun provideConverterCurrenciesAdapterItemGetter(
            viewHolderGetter: ConverterCurrenciesAdapterViewHolderGetter
    ): ConverterCurrenciesAdapterItemGetter = object : ConverterCurrenciesAdapterItemGetter {
        override fun invoke(itemModel: ConverterCurrenciesAdapterItemModel): ConverterCurrenciesAdapterItem =
                ConverterCurrenciesAdapterItem(viewHolderGetter, itemModel)
    }

    @Provides @PerConverterFragment
    fun provideConverterCurrenciesAdapterViewHolderGetter(): ConverterCurrenciesAdapterViewHolderGetter =
            object : ConverterCurrenciesAdapterViewHolderGetter {
                override fun invoke(
                        view: View, adapter: FlexibleAdapterAlias
                ): ConverterCurrenciesAdapterViewHolder = ConverterCurrenciesAdapterViewHolder(
                        view, adapter
                )
            }

}


interface ConverterCurrenciesAdapterGetter {
    val converterCurrenciesAdapter: ConverterCurrenciesAdapter
}

interface ConverterCurrenciesAdapterBaseCurrencyViewHolderGetter {
    fun invoke(view: View, adapter: FlexibleAdapterAlias): ConverterCurrenciesAdapterBaseCurrencyViewHolder
}

interface ConverterCurrenciesAdapterBaseCurrencyItemGetter {
    fun invoke(itemModel: ConverterCurrenciesAdapterBaseCurrencyItemModel): ConverterCurrenciesAdapterBaseCurrencyItem
}

interface ConverterCurrenciesAdapterViewHolderGetter {
    fun invoke(view: View, adapter: FlexibleAdapterAlias): ConverterCurrenciesAdapterViewHolder
}

interface ConverterCurrenciesAdapterItemGetter {
    fun invoke(itemModel: ConverterCurrenciesAdapterItemModel): ConverterCurrenciesAdapterItem
}