package io.bifri.currencyexchange.ui.main

import io.bifri.currencyexchange.base.viewmodel.BaseRxViewBinder
import io.bifri.currencyexchange.exception.ExceptionService
import io.reactivex.disposables.CompositeDisposable

class MainActivityVB(
        private val view: MainActivity,
        private val viewModel: MainActivityVM,
        exceptionService: ExceptionService
) : BaseRxViewBinder(exceptionService) {

    override fun bindInternal(cd: CompositeDisposable) {

    }

}