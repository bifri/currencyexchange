package io.bifri.currencyexchange.ui.main

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import io.bifri.currencyexchange.exception.ExceptionService

@Module
class MainActivityModule {

    @Provides @PerMainActivity fun provideMainActivityVM(
            activity: FragmentActivity
    ): MainActivityVM = ViewModelProvider(
            activity,
            MainActivityVMFactory(activity.application)
    )
            .get(MainActivityVM::class.java)

    @Provides @PerMainActivity fun provideMainActivityVB(
            view: MainActivity,
            viewModel: MainActivityVM,
            exceptionService: ExceptionService
    ): MainActivityVB = MainActivityVB(view, viewModel, exceptionService)

}