package io.bifri.currencyexchange.ui.main.converter

import io.bifri.currencyexchange.common.FlagResIdResolver
import io.bifri.currencyexchange.network.response.latestrates.LatestRatesResponse
import io.bifri.currencyexchange.util.primaryLocale
import io.bifri.currencyexchange.widgets.ConverterBaseCurrencyRowModel
import io.bifri.currencyexchange.widgets.ConverterCurrencyRowModel
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*


class ConverterHelper(
        private val flagResIdResolver: FlagResIdResolver
) {

    fun loadedCurrenciesToConverterState(
            previousState: ConverterState,
            loadedCurrencies: LoadedCurrencies
    ): ConverterState {
        val (baseCurrency, rawAmount, ratesResponse) = loadedCurrencies
        val amount = rawAmount.sanitizeAmount(baseCurrency)
        return previousState.copy(
                baseCurrency = toConverterCurrenciesAdapterBaseCurrencyItemModel(baseCurrency, amount),
                currencies = converterCurrenciesAdapterItemModels(baseCurrency, amount, ratesResponse)
        )
    }

    fun changingBaseCurrencyToConverterState(
            previousState: ConverterState,
            changingBaseCurrency: ChangingBaseCurrency
    ): ConverterState {
        val newCurrencies = previousState.currencies?.map {
            val converterCurrencyRowModel = it.converterCurrencyRowModel
            val newConverterCurrencyRowModel = converterCurrencyRowModel.copy(
                    isLoading = converterCurrencyRowModel.currencyCode == changingBaseCurrency.currency
            )
            it.copy(converterCurrencyRowModel = newConverterCurrencyRowModel)
        } ?: return previousState
        return previousState.copy(currencies = newCurrencies)
    }

    val startCurrency: String get() = Currency.getInstance(primaryLocale).currencyCode

    private fun converterCurrenciesAdapterItemModels(
            quoteCurrency: String,
            quoteAmount: BigDecimal,
            ratesResponse: LatestRatesResponse
    ): List<ConverterCurrenciesAdapterItemModel> {
        @Suppress("UNCHECKED_CAST")
        return ratesResponse.ratesNonNull
                .filter { (currency, _) -> currency != quoteCurrency }
                .map { (currency, rate) ->
                    toConverterCurrenciesAdapterItemModel(currency, rate, quoteCurrency, quoteAmount)
                }
    }

    private fun toConverterCurrenciesAdapterItemModel(
            currency: String,
            rate: BigDecimal,
            quoteCurrency: String,
            quoteAmount: BigDecimal
    ): ConverterCurrenciesAdapterItemModel {
        val iconResId = flagResIdResolver.flagResIdFromCurrency(currency)
        val total = quoteAmount.multiply(rate).sanitizeAmount(currency)
        return ConverterCurrenciesAdapterItemModel(
                ConverterCurrencyRowModel(
                        currency,
                        iconResId,
                        total,
                        rate,
                        quoteCurrency
                )
        )
    }

    private fun toConverterCurrenciesAdapterBaseCurrencyItemModel(
            baseCurrency: String,
            amount: BigDecimal
    ): ConverterCurrenciesAdapterBaseCurrencyItemModel {
        val iconResId = flagResIdResolver.flagResIdFromCurrency(baseCurrency)
        return ConverterCurrenciesAdapterBaseCurrencyItemModel(
                ConverterBaseCurrencyRowModel(
                        baseCurrency,
                        iconResId,
                        amount
                )
        )
    }

    private fun BigDecimal.sanitizeAmount(currency: String): BigDecimal {
        val currencyFractionDigits = Currency.getInstance(currency).defaultFractionDigits
        return setScale(currencyFractionDigits, RoundingMode.HALF_EVEN)
    }

}