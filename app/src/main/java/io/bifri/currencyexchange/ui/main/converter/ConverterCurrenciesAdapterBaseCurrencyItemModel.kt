package io.bifri.currencyexchange.ui.main.converter

import io.bifri.currencyexchange.widgets.ConverterBaseCurrencyRowModel

data class ConverterCurrenciesAdapterBaseCurrencyItemModel(
        val converterBaseCurrencyRowModel: ConverterBaseCurrencyRowModel
)