package io.bifri.currencyexchange.ui.dialog.simpleedittext

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import io.bifri.currencyexchange.R
import io.bifri.currencyexchange.base.fragment.FixedDialogFragment
import io.bifri.currencyexchange.util.DEFAULT_DIALOG_ID
import io.bifri.currencyexchange.util.showKeyboardInDialog

@StringRes const val DEFAULT_NEGATIVE_TEXT_ID: Int = android.R.string.cancel
@StringRes const val DEFAULT_POSITIVE_TEXT_ID: Int = android.R.string.ok

data class SimpleEditTextFragmentArgs(
        val dialogId: Int = DEFAULT_DIALOG_ID,
        @StringRes val titleResId: Int? = null,
        @StringRes val editTextHintResId: Int? = null,
        @StringRes val negativeTextId: Int = DEFAULT_NEGATIVE_TEXT_ID,
        @StringRes val positiveTextId: Int = DEFAULT_POSITIVE_TEXT_ID,
        @LayoutRes val layoutResId: Int = R.layout.dialog_fragment_simple_edit_text
)

class SimpleEditTextDialogFragment : FixedDialogFragment() {

    private val args: SimpleEditTextFragmentArgs by lazy { args(arguments) }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val layout = LayoutInflater.from(context)
                .inflate(args.layoutResId, null)
        val editTextLinkedCardSimpleEditText: EditText =
                layout.findViewById(R.id.editTextSimpleEditText)
        args.editTextHintResId?.let {
            editTextLinkedCardSimpleEditText.hint = getString(it)
        }
        val dialog = MaterialAlertDialogBuilder(requireContext())
                .run { args.titleResId?.let { setTitle(it) } ?: this }
                .setNegativeButton(args.negativeTextId) { dialog, _ ->
                    dialog.dismiss()
                }
                .setPositiveButton(args.positiveTextId) { dialog, _ ->
                    val text = editTextLinkedCardSimpleEditText.text?.toString()
                    listener?.onAcceptEditTextDialog(args.dialogId, text)
                    dialog.dismiss()
                }
                .setView(layout)
                .create()
        editTextLinkedCardSimpleEditText.showKeyboardInDialog(dialog)
        return dialog
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        listener?.onDismissEditTextDialog(args.dialogId)
    }

    private val listener: SimpleEditTextDialogListener?
        get() = targetFragment as? SimpleEditTextDialogListener

    companion object {
        private const val KEY_DIALOG_ID = "dialogId"
        private const val KEY_TITLE_RES_ID = "titleResId"
        private const val KEY_EDIT_TEXT_HINT_RES_ID = "editTextHintResId"
        private const val KEY_NEGATIVE_TEXT_ID = "negativeTextId"
        private const val KEY_POSITIVE_TEXT_ID = "positiveTextId"
        private const val KEY_LAYOUT_RES_ID = "layoutResId"

        fun newInstance(
                fragmentArgs: SimpleEditTextFragmentArgs,
                targetFragment: Fragment? = null,
                requestCode: Int = 0
        ): SimpleEditTextDialogFragment =
                SimpleEditTextDialogFragment().apply {
                    arguments = with(fragmentArgs) { bundleOf(
                            KEY_DIALOG_ID to dialogId,
                            KEY_TITLE_RES_ID to titleResId,
                            KEY_EDIT_TEXT_HINT_RES_ID to editTextHintResId,
                            KEY_NEGATIVE_TEXT_ID to negativeTextId,
                            KEY_POSITIVE_TEXT_ID to positiveTextId,
                            KEY_LAYOUT_RES_ID to layoutResId
                    )}
                    targetFragment?.let { setTargetFragment(it, requestCode) }
                }

        private fun args(arguments: Bundle?): SimpleEditTextFragmentArgs = arguments
                ?.let { bundle ->
                    with(bundle) {
                        SimpleEditTextFragmentArgs(
                                getInt(KEY_DIALOG_ID),
                                getInt(KEY_TITLE_RES_ID),
                                getInt(KEY_EDIT_TEXT_HINT_RES_ID).takeIf { it != 0 },
                                getInt(KEY_NEGATIVE_TEXT_ID),
                                getInt(KEY_POSITIVE_TEXT_ID),
                                getInt(KEY_LAYOUT_RES_ID)
                        )
                    }
                }
                ?: SimpleEditTextFragmentArgs()
    }

}

interface SimpleEditTextDialogListener {
    fun onDismissEditTextDialog(dialogId: Int)

    fun onAcceptEditTextDialog(dialogId: Int, text: String?)
}