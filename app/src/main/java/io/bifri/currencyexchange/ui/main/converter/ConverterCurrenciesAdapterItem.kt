package io.bifri.currencyexchange.ui.main.converter

import android.view.View
import io.bifri.currencyexchange.R
import io.bifri.currencyexchange.util.FlexibleAdapterAlias
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.flexibleadapter.items.IHolder

class ConverterCurrenciesAdapterItem(
        private val viewHolderGetter: ConverterCurrenciesAdapterViewHolderGetter,
        val itemModel: ConverterCurrenciesAdapterItemModel
) : AbstractFlexibleItem<ConverterCurrenciesAdapterViewHolder>(), IHolder<ConverterCurrenciesAdapterItemModel> {

    init {
        mSelectable = false
        mDraggable = false
        mSwipeable = false
    }

    override fun createViewHolder(
            view: View,
            adapter: FlexibleAdapterAlias
    ): ConverterCurrenciesAdapterViewHolder = viewHolderGetter.invoke(view, adapter)

    override fun bindViewHolder(
            adapter: FlexibleAdapterAlias,
            holder: ConverterCurrenciesAdapterViewHolder,
            position: Int,
            payloads: MutableList<Any>) {
        holder.bind(itemModel)
    }

    override fun getLayoutRes() = R.layout.item_converter_currency

    override fun getModel() = itemModel

    override fun shouldNotifyChange(newItem: IFlexible<*>): Boolean {
        return false
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as ConverterCurrenciesAdapterItem
        if (itemModel != other.itemModel) return false
        return true
    }

    override fun hashCode(): Int = itemModel.hashCode()

}