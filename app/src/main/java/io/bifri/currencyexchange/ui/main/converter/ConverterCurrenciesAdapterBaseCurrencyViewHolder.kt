package io.bifri.currencyexchange.ui.main.converter

import android.view.View
import eu.davidea.viewholders.FlexibleViewHolder
import io.bifri.currencyexchange.util.FlexibleAdapterAlias
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_converter_base_currency.*

class ConverterCurrenciesAdapterBaseCurrencyViewHolder(
        view: View,
        flexibleAdapter: FlexibleAdapterAlias
) : FlexibleViewHolder(view, flexibleAdapter), LayoutContainer {

    override val containerView: View? get() = contentView

    fun bind(item: ConverterCurrenciesAdapterBaseCurrencyItemModel) {
        converterBaseCurrencyRowConverterItem.setModel(item.converterBaseCurrencyRowModel)
    }

}