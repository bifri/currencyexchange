package io.bifri.currencyexchange.ui.main.converter

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import eu.davidea.flexibleadapter.FlexibleAdapter.OnItemClickListener
import eu.davidea.flexibleadapter.items.IFlexible
import io.bifri.currencyexchange.R
import io.bifri.currencyexchange.app.LinearLayoutManagerGetter
import io.bifri.currencyexchange.base.fragment.NoArgsBaseFragment
import io.bifri.currencyexchange.di.VERTICAL_LAYOUT_MANAGER
import io.bifri.currencyexchange.exception.ExceptionService
import io.bifri.currencyexchange.util.isNotZero
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_converter.*
import java.math.BigDecimal
import javax.inject.Inject
import javax.inject.Named

private const val ENTER_AMOUNT_DIALOG_ID = 1

class ConverterFragment : NoArgsBaseFragment<ConverterState>() {

    @Inject override lateinit var viewBinder: ConverterFragmentVB
    @Inject lateinit var exceptionService: ExceptionService
    @Inject lateinit var currenciesAdapterGetter: ConverterCurrenciesAdapterGetter
    @field:[Inject Named(VERTICAL_LAYOUT_MANAGER)]
    lateinit var converterRVLayoutManagerGetter: LinearLayoutManagerGetter
    @Inject lateinit var currenciesAdapterBaseCurrencyItemGetter: ConverterCurrenciesAdapterBaseCurrencyItemGetter
    @Inject lateinit var currenciesAdapterItemGetter: ConverterCurrenciesAdapterItemGetter
    private lateinit var currenciesAdapter: ConverterCurrenciesAdapter
    private val amountChangeIntent: Relay<BigDecimal> = PublishRelay.create()
    private val baseCurrencyClickIntent: Relay<Unit> = PublishRelay.create()
    private val currencyClickIntent: Relay<String> = PublishRelay.create()
    private val navigationDoneIntent: Relay<Unit> = PublishRelay.create()
    @StringRes override val titleResId: Int = R.string.converterTitle
    @LayoutRes override val layoutId = R.layout.fragment_converter
    private val currenciesAdapterOnItemClickListener = OnItemClickListener { _, position ->
        currenciesAdapter.getItem(position)?.let {
            when (val item = it as IFlexible<*>) {
                is ConverterCurrenciesAdapterBaseCurrencyItem -> onBaseCurrencyClick()
                is ConverterCurrenciesAdapterItem -> onCurrencyClick(
                        item.itemModel.converterCurrencyRowModel.currencyCode
                )
            }
        }
        true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initRecyclerView()
    }

    override fun onStart() {
        super.onStart()
        currenciesAdapter.addListener(currenciesAdapterOnItemClickListener)
    }

    override fun onStop() {
        currenciesAdapter.removeListener(currenciesAdapterOnItemClickListener)
        super.onStop()
    }

    override fun onAcceptEditTextDialog(dialogId: Int, text: String?) {
        when (dialogId) {
            ENTER_AMOUNT_DIALOG_ID -> text
                    ?.toBigDecimalOrNull()
                    ?.takeIf { it.isNotZero }
                    ?.let { onAmountChanged(it) }
        }
    }

    fun amountChangeIntent(): Observable<BigDecimal> = amountChangeIntent

    fun baseCurrencyClickIntent(): Observable<Unit> = baseCurrencyClickIntent

    fun currencyClickIntent(): Observable<String> = currencyClickIntent

    fun navigateDoneIntent(): Observable<Unit> = navigationDoneIntent

    private val onAmountChanged = amountChangeIntent::accept

    private fun onBaseCurrencyClick() = baseCurrencyClickIntent.accept(Unit)

    private val onCurrencyClick = currencyClickIntent::accept

    private fun onNavigateDone() = navigationDoneIntent.accept(Unit)

    fun render(newViewState: ConverterState) {
        fillCurrencies(newViewState)
        processNavigate(newViewState)
        this.viewState = newViewState
    }

    private fun initAdapter() {
        currenciesAdapter = currenciesAdapterGetter.converterCurrenciesAdapter
    }

    private fun initRecyclerView() {
        recyclerViewConverter.apply {
            adapter = currenciesAdapter
            layoutManager = converterRVLayoutManagerGetter.linearLayoutManager
        }
    }

    private fun fillCurrencies(newViewState: ConverterState) {
        val prevViewState = viewState

        val newBaseCurrency = newViewState.baseCurrency
        val newCurrencies = newViewState.currencies

        if (newBaseCurrency != null
                && newCurrencies != null
                && (newBaseCurrency != prevViewState?.baseCurrency
                        || newCurrencies != prevViewState.currencies
                        )
        ) {
            val adapterItems: MutableList<IFlexible<ViewHolder>> = arrayListOf()

            @Suppress("UNCHECKED_CAST")
            val newBaseItem = currenciesAdapterBaseCurrencyItemGetter.invoke(
                    newBaseCurrency
            ) as IFlexible<ViewHolder>
            adapterItems += newBaseItem

            val newItems = newCurrencies.map {
                @Suppress("UNCHECKED_CAST")
                currenciesAdapterItemGetter.invoke(it) as IFlexible<ViewHolder>
            }
            adapterItems += newItems

            currenciesAdapter.updateDataSet(adapterItems, false)
        }
    }

    private fun processNavigate(newViewState: ConverterState) {
        newViewState.navigateEvent?.getContentIfNotHandled()?.let {
            when (it) {
                EnterAmountDirection -> showSimpleEditTextDialog(
                        dialogId = ENTER_AMOUNT_DIALOG_ID,
                        layoutResId = R.layout.dialog_fragment_converter_enter_amount
                )
            }
            onNavigateDone()
        }
    }

}