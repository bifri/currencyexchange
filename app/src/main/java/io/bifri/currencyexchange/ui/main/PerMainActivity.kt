package io.bifri.currencyexchange.ui.main


import javax.inject.Scope

/**
 * Custom scope for MainActivity singletons
 */
@Scope annotation class PerMainActivity