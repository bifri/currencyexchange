package io.bifri.currencyexchange.ui.main.converter

import io.bifri.currencyexchange.widgets.ConverterCurrencyRowModel

data class ConverterCurrenciesAdapterItemModel(
        val converterCurrencyRowModel: ConverterCurrencyRowModel
)