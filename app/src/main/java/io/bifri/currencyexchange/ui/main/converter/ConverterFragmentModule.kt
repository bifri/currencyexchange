package io.bifri.currencyexchange.ui.main.converter

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import io.bifri.currencyexchange.common.FlagResIdResolver
import io.bifri.currencyexchange.exception.ExceptionService
import io.bifri.currencyexchange.network.processor.SimpleProcessor

@Module(includes = [ ConverterRecyclerViewModule::class ])
class ConverterFragmentModule {

    @Provides @PerConverterFragment fun provideConverterFragmentVM(
            activity: FragmentActivity,
            fragment: ConverterFragment,
            exceptionService: ExceptionService,
            simpleProcessor: SimpleProcessor,
            converterHelper: ConverterHelper
    ): ConverterFragmentVM = ViewModelProvider(
            fragment,
            ConverterFragmentVMFactory(
                    activity.application,
                    exceptionService,
                    simpleProcessor,
                    converterHelper
            )
    )
            .get(ConverterFragmentVM::class.java)

    @Provides @PerConverterFragment fun provideConverterFragmentVB(
            view: ConverterFragment,
            viewModel: ConverterFragmentVM,
            exceptionService: ExceptionService
    ): ConverterFragmentVB = ConverterFragmentVB(view, viewModel, exceptionService)

    @Provides @PerConverterFragment fun provideConverterHelper(
            flagResIdResolver: FlagResIdResolver
    ): ConverterHelper = ConverterHelper(flagResIdResolver)

}