package io.bifri.currencyexchange.ui.main.converter

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import eu.davidea.flexibleadapter.items.IFlexible
import io.bifri.currencyexchange.common.adapter.StableStringIdAdapter
import io.bifri.currencyexchange.common.adapter.StableStringIdHelper


class ConverterCurrenciesAdapter(
        stableStringIdHelper: StableStringIdHelper,
        items: List<IFlexible<ViewHolder>>?,
        listeners: Any?,
        stableIds: Boolean
) : StableStringIdAdapter<IFlexible<ViewHolder>, ViewHolder>(
        stableStringIdHelper, items, listeners, stableIds
) {

    init {
        setNotifyChangeOfUnfilteredItems(false)
    }

    override fun getItemId(position: Int): Long = (getItem(position) as IFlexible<*>?)?.let {
        when (it) {
            is ConverterCurrenciesAdapterBaseCurrencyItem -> stableStringIdHelper.longId(
                    it.itemModel.converterBaseCurrencyRowModel.currencyCode
            )
            is ConverterCurrenciesAdapterItem -> stableStringIdHelper.longId(
                    it.itemModel.converterCurrencyRowModel.currencyCode
            )
            else -> throw IllegalArgumentException()
        }
    }
            ?: RecyclerView.NO_ID
}