package io.bifri.currencyexchange.ui.main.converter

import io.bifri.currencyexchange.util.Event

data class ConverterState(
        val baseCurrency: ConverterCurrenciesAdapterBaseCurrencyItemModel? = null,
        val currencies: List<ConverterCurrenciesAdapterItemModel>? = null,
        val navigateEvent: Event<Direction>? = null
)