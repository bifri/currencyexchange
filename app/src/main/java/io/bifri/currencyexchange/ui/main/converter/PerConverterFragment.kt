package io.bifri.currencyexchange.ui.main.converter


import javax.inject.Scope

/**
 * Custom scope for ConverterFragment singletons
 */
@Scope annotation class PerConverterFragment