package io.bifri.currencyexchange.ui.main.converter

import android.view.View
import eu.davidea.viewholders.FlexibleViewHolder
import io.bifri.currencyexchange.util.FlexibleAdapterAlias
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_converter_currency.*

class ConverterCurrenciesAdapterViewHolder(
        view: View,
        flexibleAdapter: FlexibleAdapterAlias
) : FlexibleViewHolder(view, flexibleAdapter), LayoutContainer {

    override val containerView: View? get() = contentView

    fun bind(item: ConverterCurrenciesAdapterItemModel) {
        converterCurrencyRowConverterItem.setModel(item.converterCurrencyRowModel)
    }

}