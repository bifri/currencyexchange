package io.bifri.currencyexchange.ui.main

import dagger.Module
import dagger.Provides
import io.bifri.currencyexchange.network.paymentsservice.ExchangeRatesApi
import io.bifri.currencyexchange.network.processor.ApiExceptionProcessor
import io.bifri.currencyexchange.network.processor.SimpleProcessor

@Module class CommonModule {

    @Provides @PerMainActivity fun provideSimpleProcessor(
            apiExceptionProcessor: ApiExceptionProcessor,
            exchangeRatesApi: ExchangeRatesApi
    ): SimpleProcessor = SimpleProcessor(apiExceptionProcessor, exchangeRatesApi)

}