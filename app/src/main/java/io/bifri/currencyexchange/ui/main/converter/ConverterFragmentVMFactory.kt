package io.bifri.currencyexchange.ui.main.converter

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.bifri.currencyexchange.exception.ExceptionService
import io.bifri.currencyexchange.network.processor.SimpleProcessor

class ConverterFragmentVMFactory(
        private val app: Application,
        private val exceptionService: ExceptionService,
        private val simpleProcessor: SimpleProcessor,
        private val converterHelper: ConverterHelper
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return ConverterFragmentVM(
                app,
                exceptionService,
                simpleProcessor,
                converterHelper
        ) as T
    }

}