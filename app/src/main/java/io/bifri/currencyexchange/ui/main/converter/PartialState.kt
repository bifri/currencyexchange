package io.bifri.currencyexchange.ui.main.converter

import io.bifri.currencyexchange.network.response.latestrates.LatestRatesResponse
import java.math.BigDecimal

sealed class PartialState

data class ChangingBaseCurrency(val currency: String) : PartialState()

data class LoadedCurrencies(
        val baseCurrency: String,
        val amount: BigDecimal,
        val ratesResponse: LatestRatesResponse
) : PartialState()

data class Navigate(val direction: Direction) : PartialState()

object NavigateDone : PartialState()


sealed class Direction

object EnterAmountDirection : Direction()