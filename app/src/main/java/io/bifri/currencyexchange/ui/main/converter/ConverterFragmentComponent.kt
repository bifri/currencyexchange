package io.bifri.currencyexchange.ui.main.converter

import dagger.BindsInstance
import dagger.Subcomponent

@PerConverterFragment
@Subcomponent(modules = [ ConverterFragmentModule::class ])
interface ConverterFragmentComponent {

    @Subcomponent.Builder interface Builder {
        @BindsInstance fun converterFragment(fragment: ConverterFragment): Builder

        fun build(): ConverterFragmentComponent
    }

    fun inject(converterFragment: ConverterFragment)

}
