package io.bifri.currencyexchange.ui.main.converter

import io.bifri.currencyexchange.base.viewmodel.BaseRxViewBinder
import io.bifri.currencyexchange.exception.ExceptionService
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

class ConverterFragmentVB(
        private val view: ConverterFragment,
        private val viewModel: ConverterFragmentVM,
        exceptionService: ExceptionService
) : BaseRxViewBinder(exceptionService) {

    override fun bindInternal(cd: CompositeDisposable) {
        cd.addAll(
                view.lifecycle
                        .subscribeBy(
                                onNext = viewModel.onLifecycleEvent,
                                onError = exceptionService::processException
                        ),
                view.amountChangeIntent()
                        .subscribeBy(
                                onNext = viewModel.onAmountChange,
                                onError = exceptionService::processException
                        ),
                view.baseCurrencyClickIntent()
                        .subscribeBy(
                                onNext = viewModel.onBaseCurrencyClick,
                                onError = exceptionService::processException
                        ),
                view.currencyClickIntent()
                        .subscribeBy(
                                onNext = viewModel.onCurrencyClick,
                                onError = exceptionService::processException
                        ),
                view.navigateDoneIntent()
                        .subscribeBy(
                                onNext = viewModel.onNavigateDone,
                                onError = exceptionService::processException
                        ),
                viewModel.converterState()
                        .subscribeBy(
                                onNext = view::render,
                                onError = exceptionService::processException
                        )
        )
    }

}