package io.bifri.currencyexchange.app

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import io.bifri.currencyexchange.di.APP_CONTEXT
import io.bifri.currencyexchange.di.APP_INJECTOR
import io.bifri.currencyexchange.di.PerApp
import io.bifri.currencyexchange.exception.ExceptionModule
import io.bifri.currencyexchange.network.NetworkModule
import io.bifri.currencyexchange.util.RxCurrentActivityLifecycleCallbacks
import io.bifri.currencyexchange.util.UtilModule
import javax.inject.Named

@Module(includes = [
    ExceptionModule::class,
    NetworkModule::class,
    UtilModule::class
])
class AppModule {

    @Provides @PerApp fun provideApplication(app: App): Application {
        return app
    }

    @Provides @PerApp @Named(APP_CONTEXT)
    fun provideApplicationContext(application: Application): Context =
            application.applicationContext

    @Provides @PerApp
    fun provideRxCurrentActivityLifecycleCallbacks(app: App): RxCurrentActivityLifecycleCallbacks =
            RxCurrentActivityLifecycleCallbacks(app)

    @Provides @PerApp fun provideObjectMapper() = ObjectMapperGetter().objectMapper

    @Provides @PerApp @Named(APP_INJECTOR) fun provideAppInjector(app: App): IAppInjector {
        return AppInjector(app)
    }

}