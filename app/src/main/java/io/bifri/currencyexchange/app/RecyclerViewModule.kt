package io.bifri.currencyexchange.app

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import dagger.Module
import dagger.Provides
import io.bifri.currencyexchange.common.adapter.StableStringIdHelper
import io.bifri.currencyexchange.di.ACTIVITY_CONTEXT
import io.bifri.currencyexchange.di.PerController
import io.bifri.currencyexchange.di.VERTICAL_LAYOUT_MANAGER
import javax.inject.Named

@Module class RecyclerViewModule {

    @Provides @PerController @Named(VERTICAL_LAYOUT_MANAGER)
    fun provideLinearLayoutManagerGetter(
            @Named(ACTIVITY_CONTEXT) context: Context
    ): LinearLayoutManagerGetter = object : LinearLayoutManagerGetter {
        override val linearLayoutManager
            get() = LinearLayoutManager(context)
    }

    @Provides @PerController fun provideLinearSnapHelperGetter(): LinearSnapHelperGetter =
            object : LinearSnapHelperGetter {
                override val linearSnapHelper: LinearSnapHelper get() = LinearSnapHelper()
            }

    @Provides @PerController fun provideStableStringIdGetter(): StableStringIdGetter =
            object : StableStringIdGetter {
                override val stableStringIdHelper: StableStringIdHelper
                    get() = StableStringIdHelper()
            }

}

interface LinearLayoutManagerGetter {
    val linearLayoutManager: LinearLayoutManager
}

interface LinearSnapHelperGetter {
    val linearSnapHelper: LinearSnapHelper
}

interface StableStringIdGetter {
    val stableStringIdHelper: StableStringIdHelper
}