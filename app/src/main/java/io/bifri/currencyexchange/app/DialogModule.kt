package io.bifri.currencyexchange.app

import androidx.fragment.app.Fragment
import dagger.Module
import dagger.Provides
import io.bifri.currencyexchange.di.PerController
import io.bifri.currencyexchange.ui.dialog.simpleedittext.SimpleEditTextDialogFragment
import io.bifri.currencyexchange.ui.dialog.simpleedittext.SimpleEditTextFragmentArgs

@Module class DialogModule {

    @Provides @PerController fun provideSimpleEditTextDialogGetter(): SimpleEditTextDialogGetter =
            object : SimpleEditTextDialogGetter {
                override fun newInstance(
                        fragmentArgs: SimpleEditTextFragmentArgs,
                        targetFragment: Fragment?,
                        requestCode: Int
                ): SimpleEditTextDialogFragment = SimpleEditTextDialogFragment.newInstance(
                        fragmentArgs, targetFragment, requestCode
                )
            }

}

interface SimpleEditTextDialogGetter {
    fun newInstance(
            fragmentArgs: SimpleEditTextFragmentArgs = SimpleEditTextFragmentArgs(),
            targetFragment: Fragment? = null,
            requestCode: Int = 0
    ): SimpleEditTextDialogFragment
}