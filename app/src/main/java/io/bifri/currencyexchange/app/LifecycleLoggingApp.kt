package io.bifri.currencyexchange.app

import android.content.Context
import android.content.res.Configuration
import androidx.multidex.MultiDexApplication
import io.bifri.currencyexchange.util.logi

abstract class LifecycleLoggingApp : MultiDexApplication() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        logi("attachBaseContext()")
    }

    override fun onCreate() {
        super.onCreate()
        logi("onCreate()")
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        logi("onConfigurationChanged()")
    }

}