package io.bifri.currencyexchange.app

import androidx.fragment.app.FragmentActivity
import dagger.BindsInstance
import dagger.Subcomponent
import io.bifri.currencyexchange.di.PerController
import io.bifri.currencyexchange.ui.main.MainActivityComponent

@PerController @Subcomponent(modules = [ ControllerModule::class ])
interface ControllerComponent {

    @Subcomponent.Builder interface Builder {
        @BindsInstance fun activity(activity: FragmentActivity): Builder

        fun build(): ControllerComponent
    }

    fun newMainActivityComponentBuilder(): MainActivityComponent.Builder

}