package io.bifri.currencyexchange.app

import io.bifri.currencyexchange.di.PerWorker
import dagger.Subcomponent

@PerWorker @Subcomponent interface WorkerComponent {

    @Subcomponent.Builder interface Builder {
        fun build(): WorkerComponent
    }

//  fun inject(sendMessagesWorker: SendMessagesWorker)

}