package io.bifri.currencyexchange.app

import io.bifri.currencyexchange.di.PerService
import dagger.Subcomponent

@PerService @Subcomponent interface ServiceComponent {

    @Subcomponent.Builder interface Builder {
        fun build(): ServiceComponent
    }

//  fun inject(service: MyService)

}