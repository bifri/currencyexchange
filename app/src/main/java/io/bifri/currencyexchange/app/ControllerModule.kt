package io.bifri.currencyexchange.app

import android.content.Context
import androidx.fragment.app.FragmentActivity
import dagger.Module
import dagger.Provides
import io.bifri.currencyexchange.di.ACTIVITY_CONTEXT
import io.bifri.currencyexchange.di.PerController
import javax.inject.Named

@Module(includes = [
    RecyclerViewModule::class,
    DialogModule::class
])
class ControllerModule {

    @Provides @PerController @Named(ACTIVITY_CONTEXT)
    fun provideActivityContext(activity: FragmentActivity): Context = activity

}