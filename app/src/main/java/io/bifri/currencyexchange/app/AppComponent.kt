package io.bifri.currencyexchange.app

import io.bifri.currencyexchange.di.PerApp
import dagger.BindsInstance
import dagger.Component

@PerApp @Component(modules = [ AppModule::class ])
interface AppComponent {

    @Component.Builder interface Builder {
        @BindsInstance fun application(app: App): AppComponent.Builder

        fun build(): AppComponent
    }

    fun newControllerComponentBuilder(): ControllerComponent.Builder

    fun newServiceComponentBuilder(): ServiceComponent.Builder

    fun newWorkerComponentBuilder(): WorkerComponent.Builder

    fun inject(app: App)

}
