package io.bifri.currencyexchange.app

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import io.bifri.currencyexchange.di.APP_INJECTOR
import io.bifri.currencyexchange.di.ComponentReflectionInjector
import io.bifri.currencyexchange.di.Injector
import io.bifri.currencyexchange.exception.SimpleApiErrorAndExceptionHandler
import javax.inject.Inject
import javax.inject.Named

class App : LifecycleLoggingApp(), IAppInjector {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder().application(this).build()
    }
    private val appComponentInjector: Injector by lazy {
        ComponentReflectionInjector(AppComponent::class.java, appComponent)
    }
    @field:[Inject Named(APP_INJECTOR)] lateinit var appInjector: IAppInjector
    @Inject lateinit var simpleApiErrorHandler: SimpleApiErrorAndExceptionHandler
    @Inject lateinit var uncaughtExceptionHandler: Thread.UncaughtExceptionHandler

    override fun onCreate() {
        super.onCreate()
        appComponentInjector.inject(this)
        Thread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler)
        simpleApiErrorHandler.register()
    }

    override fun activityComponentInjector(target: FragmentActivity): ComponentInjector =
            appInjector.activityComponentInjector(target)

    override fun fragmentInjector(target: Fragment, activityComponent: Any): Injector =
            appInjector.fragmentInjector(target, activityComponent)


}