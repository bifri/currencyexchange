package io.bifri.currencyexchange.app

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import io.bifri.currencyexchange.di.ComponentReflectionInjector
import io.bifri.currencyexchange.di.Injector
import io.bifri.currencyexchange.ui.main.MainActivity
import io.bifri.currencyexchange.ui.main.MainActivityComponent
import io.bifri.currencyexchange.ui.main.converter.ConverterFragment
import io.bifri.currencyexchange.ui.main.converter.ConverterFragmentComponent

class AppInjector(val app: App) : IAppInjector {

    override fun activityComponentInjector(target: FragmentActivity): ComponentInjector {
        val controllerComponent = app.appComponent
                .newControllerComponentBuilder()
                .activity(target)
                .build()
        return when (target::class) {
            MainActivity::class -> {
                val activityComponent = controllerComponent
                        .newMainActivityComponentBuilder()
                        .mainActivity(target as MainActivity)
                        .build()
                ComponentInjector(
                        activityComponent,
                        ComponentReflectionInjector(
                                MainActivityComponent::class.java,
                                activityComponent
                        )
                )
            }
            else -> throw IllegalArgumentException("Unsupported injection target")
        }
    }

    override fun fragmentInjector(target: Fragment, activityComponent: Any): Injector =
            when (target::class) {
                ConverterFragment::class -> ComponentReflectionInjector(
                        ConverterFragmentComponent::class.java,
                        (activityComponent as MainActivityComponent)
                                .newConverterFragmentComponentBuilder()
                                .converterFragment(target as ConverterFragment)
                                .build()
                )
                else -> throw IllegalArgumentException("Unsupported injection target: ${target::class}")
            }

}