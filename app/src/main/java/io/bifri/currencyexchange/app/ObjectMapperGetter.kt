package io.bifri.currencyexchange.app

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature.*
import com.fasterxml.jackson.databind.MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import java.util.*


class ObjectMapperGetter {

        val objectMapper: ObjectMapper get() = ObjectMapper()
                .enable(ACCEPT_CASE_INSENSITIVE_ENUMS)
                .enable(
                        READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE,
                        READ_UNKNOWN_ENUM_VALUES_AS_NULL
                )
                .disable(FAIL_ON_UNKNOWN_PROPERTIES)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .setLocale(Locale.ENGLISH)
                .registerKotlinModule()

}