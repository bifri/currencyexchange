# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Generic rule which forces Proguard to keep all classes with public and protected fields
# https://github.com/saschpe/android-customtabs/issues/25
# FIXME: replace it later!
-keep public class * {
      public protected *;
}

# The "Exceptions" attribute has to be preserved, so the compiler knows
# which exceptions methods may throw.
# The "Signature" attribute is required to be able to access generic types
# when compiling in JDK 5.0 and higher.
# Keeping the "Deprecated" attribute and the attributes for producing
# useful stack traces
# EnclosingMethod: some code may make further use of introspection to figure
# out the enclosing methods of anonymous inner classes.
# The "InnerClasses" attribute (or more precisely, its source name part) has
# to be preserved too, for any inner classes that can be referenced from
# outside the library.
-keepattributes Exceptions,Signature,Deprecated,EnclosingMethod,InnerClasses

# Fix overly aggressive shrinking of class annotations.
# https://stackoverflow.com/questions/7378693/proguard-vs-annotations
-keepattributes *Annotation*

# Uncomment this to preserve the line number information for
# debugging stack traces.
-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
-renamesourcefileattribute SourceFile

-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int d(...);
    public static int w(...);
    public static int v(...);
    public static int i(...);
}

-assumenosideeffects class io.bifri.currencyexchange.util.LogKt {
    public static void logd(...);
    public static void logw(...);
    public static void logv(...);
    public static void logi(...);
}

# The -keepclassmembers option makes sure that any serialization methods are kept.
# By using this option instead of the basic -keep option, we're not forcing
# preservation of all serializable classes, just preservation of the listed members
# of classes that are actually used.
# https://www.guardsquare.com/en/products/proguard/manual/examples
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

# Keep enum constants and fields
# https://stackoverflow.com/questions/33189249/how-to-tell-proguard-to-keep-enum-constants-and-fields
-keepclassmembers,allowoptimization enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# Preserve all .class method names.
# https://searchfox.org/mozilla-central/source/mobile/android/geckoview/proguard-rules.txt
-keepclassmembernames class * {
    java.lang.Class class$(java.lang.String);
    java.lang.Class class$(java.lang.String, boolean);
}

# This is typically useful when keeping native method names, to make sure that
# the parameter types of native methods aren't renamed either. Their signatures
# then remain completely unchanged and compatible with the native libraries.
-keepclasseswithmembernames,includedescriptorclasses class * {
    native <methods>;
}

# Kotlin
-keep class kotlin.** { *; }
-keep class kotlin.Metadata { *; }
-dontwarn kotlin.**
-keepclassmembers class **$WhenMappings {
    <fields>;
}
-keepclassmembers class kotlin.Metadata {
    public <methods>;
}
-assumenosideeffects class kotlin.jvm.internal.Intrinsics {
  public static void checkExpressionValueIsNotNull(java.lang.Object, java.lang.String);
  public static void checkFieldIsNotNull(java.lang.Object, java.lang.String);
  public static void checkFieldIsNotNull(java.lang.Object, java.lang.String, java.lang.String);
  public static void checkNotNull(java.lang.Object);
  public static void checkNotNull(java.lang.Object, java.lang.String);
  public static void checkNotNullExpressionValue(java.lang.Object, java.lang.String);
  public static void checkNotNullParameter(java.lang.Object, java.lang.String);
  public static void checkParameterIsNotNull(java.lang.Object, java.lang.String);
  public static void checkReturnedValueIsNotNull(java.lang.Object, java.lang.String);
  public static void checkReturnedValueIsNotNull(java.lang.Object, java.lang.String, java.lang.String);
}

# Jackson 2.x
# https://github.com/FasterXML/jackson-docs/wiki/JacksonOnAndroid
-keep class com.fasterxml.jackson.databind.ObjectMapper {
    public <methods>;
    protected <methods>;
}
-keep class com.fasterxml.jackson.databind.ObjectWriter {
    public ** writeValueAsString(**);
}
-keepnames class com.fasterxml.jackson.** { *; }
-dontwarn com.fasterxml.jackson.databind.**
-keepclassmembers class * {
     @com.fasterxml.jackson.annotation.* *;
}

# Jackson Kotlin module
# https://github.com/FasterXML/jackson-module-kotlin
-keep class kotlin.Metadata { *; }

# Crashlytics
# https://docs.fabric.io/android/crashlytics/dex-and-proguard.html
# If you are using custom exceptions, add this line so that custom exception
# types are skipped during obfuscation:
-keep public class * extends java.lang.Exception
# For Fabric to properly de-obfuscate your crash reports, you need to remove
# this line from your ProGuard config:
# -printmapping mapping.txt

# Glide
# https://github.com/bumptech/glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

# OkHttp
# https://github.com/square/okhttp#proguard
# JSR 305 annotations are for embedding nullability information.
-dontwarn javax.annotation.**
# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase
# Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn org.codehaus.mojo.animal_sniffer.*
# OkHttp platform used only on JVM and when Conscrypt dependency is available.
-dontwarn okhttp3.internal.platform.ConscryptPlatform

# Okio
# https://github.com/square/okio
-dontwarn okio.**

# Logback for Android
# https://github.com/krschultz/android-proguard-snippets/blob/master/libraries/proguard-logback-android.pro
# Tested on the following *.gradle dependencies#
#    compile 'org.slf4j:slf4j-api:1.7.7'
#    compile 'com.github.tony19:logback-android-core:1.1.1-3'
#    compile 'com.github.tony19:logback-android-classic:1.1.1-3'
#
-keep class ch.qos.** { *; }
-keep class org.slf4j.** { *; }
-dontwarn ch.qos.logback.core.net.*

### Nothing for Butterknife 8, RxJava2, RxBinding, RxRelay, Dagger2,
# Google Play Services, Firebase, Room DB